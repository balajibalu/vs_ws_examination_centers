package com.codetree.verification.models.vilages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Masterlist {
    @SerializedName("LGD_PANCHAYAT_CODE")
    @Expose
    private String lGDPANCHAYATCODE;
    @SerializedName("PANCHAYAT_NAME")
    @Expose
    private String pANCHAYATNAME;

    public String getLGDPANCHAYATCODE() {
        return lGDPANCHAYATCODE;
    }

    public void setLGDPANCHAYATCODE(String lGDPANCHAYATCODE) {
        this.lGDPANCHAYATCODE = lGDPANCHAYATCODE;
    }

    public String getPANCHAYATNAME() {
        return pANCHAYATNAME;
    }

    public void setPANCHAYATNAME(String pANCHAYATNAME) {
        this.pANCHAYATNAME = pANCHAYATNAME;
    }}

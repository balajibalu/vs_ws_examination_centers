package com.codetree.verification.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstituteInput {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("DISTRICT_CODE")
    @Expose
    private String dISTRICTCODE;
    @SerializedName("MANDAL_CODE")
    @Expose
    private String mANDALCODE;
    @SerializedName("RU_FLAG")
    @Expose
    private String rUFLAG;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDISTRICTCODE() {
        return dISTRICTCODE;
    }

    public void setDISTRICTCODE(String dISTRICTCODE) {
        this.dISTRICTCODE = dISTRICTCODE;
    }

    public String getMANDALCODE() {
        return mANDALCODE;
    }

    public void setMANDALCODE(String mANDALCODE) {
        this.mANDALCODE = mANDALCODE;
    }

    public String getRUFLAG() {
        return rUFLAG;
    }

    public void setRUFLAG(String rUFLAG) {
        this.rUFLAG = rUFLAG;
    }
}

package com.codetree.verification.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("NAME")
    private String nAME;
    @SerializedName("DESIGNATION")
    private String dESIGNATION;
    @SerializedName("DISTRICT_NAME")
    private String dISTRICTNAME;
    @SerializedName("DISTRICT_CODE")
    private String dISTRICTCODE;
    @SerializedName("MANDAL_NAME")
    private String mANDALNAME;
    @SerializedName("MANDAL_CODE")
    private String mANDALCODE;
    @SerializedName("RURAL_URBAN")
    private String rURALURBAN;

    public String getNAME() {
        return nAME;
    }

    public void setNAME(String nAME) {
        this.nAME = nAME;
    }

    public String getDESIGNATION() {
        return dESIGNATION;
    }

    public void setDESIGNATION(String dESIGNATION) {
        this.dESIGNATION = dESIGNATION;
    }

    public String getDISTRICTNAME() {
        return dISTRICTNAME;
    }

    public void setDISTRICTNAME(String dISTRICTNAME) {
        this.dISTRICTNAME = dISTRICTNAME;
    }

    public String getDISTRICTCODE() {
        return dISTRICTCODE;
    }

    public void setDISTRICTCODE(String dISTRICTCODE) {
        this.dISTRICTCODE = dISTRICTCODE;
    }

    public String getMANDALNAME() {
        return mANDALNAME;
    }

    public void setMANDALNAME(String mANDALNAME) {
        this.mANDALNAME = mANDALNAME;
    }

    public String getMANDALCODE() {
        return mANDALCODE;
    }

    public void setMANDALCODE(String mANDALCODE) {
        this.mANDALCODE = mANDALCODE;
    }

    public String getRURALURBAN() {
        return rURALURBAN;
    }

    public void setRURALURBAN(String rURALURBAN) {
        this.rURALURBAN = rURALURBAN;
    }
}

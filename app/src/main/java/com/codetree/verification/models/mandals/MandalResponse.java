package com.codetree.verification.models.mandals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MandalResponse {
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("Masterlist")
    @Expose
    private List<Masterlist> masterlist = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<Masterlist> getMasterlist() {
        return masterlist;
    }

    public void setMasterlist(List<Masterlist> masterlist) {
        this.masterlist = masterlist;
    }
}

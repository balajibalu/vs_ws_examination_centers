package com.codetree.verification.models.mandals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Masterlist {

    @SerializedName("LGD_MANDAL_CODE")
    @Expose
    private String lGDMANDALCODE;
    @SerializedName("MANDAL_NAME")
    @Expose
    private String mANDALNAME;

    public String getLGDMANDALCODE() {
        return lGDMANDALCODE;
    }

    public void setLGDMANDALCODE(String lGDMANDALCODE) {
        this.lGDMANDALCODE = lGDMANDALCODE;
    }

    public String getMANDALNAME() {
        return mANDALNAME;
    }

    public void setMANDALNAME(String mANDALNAME) {
        this.mANDALNAME = mANDALNAME;
    }

}

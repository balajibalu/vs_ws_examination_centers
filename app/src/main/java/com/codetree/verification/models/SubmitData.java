package com.codetree.verification.models;

public class SubmitData {
    private String submt_Mobile_No;

    private String districtCode;

    private String ruflag;

    private String instituteCode;

    private String locality_code;

    private String landMark;

    private String villageCode;

    private String buildingName;

    private String geO_LAT;

    private String geO_LONG;

    private String streetName;

    private String questioN_3;

    private String mandalCode;

    private String questioN_4;

    private String image_Path;

    private String questioN_1;

    private String questioN_2;

    private String centeR_CATEGORY;

    private String NO_OF_CANDIDATES;

    private String GroundFloorRooms;

    private String GroundPersonsPerroom;

    private String OtherFloorRooms;

    private String OtherPersonsPerroom;




    public String getQuestioN_5() {
        return questioN_5;
    }

    public void setQuestioN_5(String questioN_5) {
        this.questioN_5 = questioN_5;
    }

    public String getQuestioN_6() {
        return questioN_6;
    }

    public void setQuestioN_6(String questioN_6) {
        this.questioN_6 = questioN_6;
    }

    private String questioN_5;

    private String questioN_6;

    public String getSubmt_Mobile_No() {
        return submt_Mobile_No;
    }

    public void setSubmt_Mobile_No(String submt_Mobile_No) {
        this.submt_Mobile_No = submt_Mobile_No;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getRuflag() {
        return ruflag;
    }

    public void setRuflag(String ruflag) {
        this.ruflag = ruflag;
    }

    public String getInstituteCode() {
        return instituteCode;
    }

    public void setInstituteCode(String instituteCode) {
        this.instituteCode = instituteCode;
    }

    public String getLocality_code() {
        return locality_code;
    }

    public void setLocality_code(String locality_code) {
        this.locality_code = locality_code;
    }

    public String getLandMark() {
        return landMark;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public String getVillageCode() {
        return villageCode;
    }

    public void setVillageCode(String villageCode) {
        this.villageCode = villageCode;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getGeO_LAT() {
        return geO_LAT;
    }

    public void setGeO_LAT(String geO_LAT) {
        this.geO_LAT = geO_LAT;
    }

    public String getGeO_LONG() {
        return geO_LONG;
    }

    public void setGeO_LONG(String geO_LONG) {
        this.geO_LONG = geO_LONG;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getQuestioN_3() {
        return questioN_3;
    }

    public void setQuestioN_3(String questioN_3) {
        this.questioN_3 = questioN_3;
    }

    public String getMandalCode() {
        return mandalCode;
    }

    public void setMandalCode(String mandalCode) {
        this.mandalCode = mandalCode;
    }

    public String getQuestioN_4() {
        return questioN_4;
    }

    public void setQuestioN_4(String questioN_4) {
        this.questioN_4 = questioN_4;
    }

    public String getImage_Path() {
        return image_Path;
    }

    public void setImage_Path(String image_Path) {
        this.image_Path = image_Path;
    }

    public String getQuestioN_1() {
        return questioN_1;
    }

    public void setQuestioN_1(String questioN_1) {
        this.questioN_1 = questioN_1;
    }

    public String getQuestioN_2() {
        return questioN_2;
    }

    public void setQuestioN_2(String questioN_2) {
        this.questioN_2 = questioN_2;
    }

    public String getCenteR_CATEGORY() {
        return centeR_CATEGORY;
    }

    public void setCenteR_CATEGORY(String centeR_CATEGORY) {
        this.centeR_CATEGORY = centeR_CATEGORY;
    }

    @Override
    public String toString() {
        return "ClassPojo [submt_Mobile_No = " + submt_Mobile_No + ", districtCode = " + districtCode + ", ruflag = " + ruflag +
                ", instituteCode = " + instituteCode + ", locality_code = " + locality_code + ", landMark = " + landMark +
                ", villageCode = " + villageCode + ", buildingName = " + buildingName + ", geO_LAT = " + geO_LAT + ", geO_LONG = " +
                geO_LONG + ", streetName = " + streetName + ", questioN_3 = " + questioN_3 + ", mandalCode = " + mandalCode +
                ", questioN_4 = " + questioN_4 + ", image_Path = " + image_Path + ", questioN_1 = " + questioN_1 + ", questioN_2 = " +
                questioN_2 + ", centeR_CATEGORY = " + centeR_CATEGORY + "]";
    }

    public String getNO_OF_CANDIDATES() {
        return NO_OF_CANDIDATES;
    }

    public void setNO_OF_CANDIDATES(String NO_OF_CANDIDATES) {
        this.NO_OF_CANDIDATES = NO_OF_CANDIDATES;
    }

    public String getGroundFloorRooms() {
        return GroundFloorRooms;
    }

    public void setGroundFloorRooms(String groundFloorRooms) {
        GroundFloorRooms = groundFloorRooms;
    }


    public String getGroundPersonsPerroom() {
        return GroundPersonsPerroom;
    }

    public void setGroundPersonsPerroom(String groundPersonsPerroom) {
        GroundPersonsPerroom = groundPersonsPerroom;
    }

    public String getOtherFloorRooms() {
        return OtherFloorRooms;
    }

    public void setOtherFloorRooms(String otherFloorRooms) {
        OtherFloorRooms = otherFloorRooms;
    }

    public String getOtherPersonsPerroom() {
        return OtherPersonsPerroom;
    }

    public void setOtherPersonsPerroom(String otherPersonsPerroom) {
        OtherPersonsPerroom = otherPersonsPerroom;
    }
}

package com.codetree.verification.models.localities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Masterlist {
    @SerializedName("LOCALITY_CODE")
    @Expose
    private String lOCALITYCODE;
    @SerializedName("LOCALITY_NAME")
    @Expose
    private String lOCALITYNAME;

    public String getLOCALITYCODE() {
        return lOCALITYCODE;
    }

    public void setLOCALITYCODE(String lOCALITYCODE) {
        this.lOCALITYCODE = lOCALITYCODE;
    }

    public String getLOCALITYNAME() {
        return lOCALITYNAME;
    }

    public void setLOCALITYNAME(String lOCALITYNAME) {
        this.lOCALITYNAME = lOCALITYNAME;
    }
}

package com.codetree.verification.models.mandals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasterRequest {

    @SerializedName("ftype")
    @Expose
    private String ftype;
    @SerializedName("Dcode")
    @Expose
    private String dcode;
    @SerializedName("ruflag")
    @Expose
    private String ruflag;
    @SerializedName("Mcode")
    @Expose
    private String mcode;
    @SerializedName("Vcode")
    @Expose
    private String vcode;

    public String getFtype() {
        return ftype;
    }

    public void setFtype(String ftype) {
        this.ftype = ftype;
    }

    public String getDcode() {
        return dcode;
    }

    public void setDcode(String dcode) {
        this.dcode = dcode;
    }

    public String getRuflag() {
        return ruflag;
    }

    public void setRuflag(String ruflag) {
        this.ruflag = ruflag;
    }

    public String getMcode() {
        return mcode;
    }

    public void setMcode(String mcode) {
        this.mcode = mcode;
    }

    public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }

}

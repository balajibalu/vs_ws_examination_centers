package com.codetree.verification.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmitResponse {
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("HttpStatus")
    @Expose
    private Integer httpStatus;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("smsstatus")
    @Expose
    private Object smsstatus;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Object getSmsstatus() {
        return smsstatus;
    }

    public void setSmsstatus(Object smsstatus) {
        this.smsstatus = smsstatus;
    }
}

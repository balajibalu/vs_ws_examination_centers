package com.codetree.verification.models.institute_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("INSTITUTION_BIILDING_NAME")
    @Expose
    private String iNSTITUTIONBIILDINGNAME;
    @SerializedName("INSTITUTE_ID")
    @Expose
    private String iNSTITUTEID;
    @SerializedName("NO_OF_CANDIDATES")
    @Expose
    private String nOOFCANDIDATES;
    @SerializedName("ELECTRICITY_AVAILABILITY")
    @Expose
    private String eLECTRICITYAVAILABILITY;
    @SerializedName("FURNITURE_AVAILABILITY")
    @Expose
    private String fURNITUREAVAILABILITY;
    @SerializedName("DRINKING_WATER_AVAILABILITY")
    @Expose
    private String dRINKINGWATERAVAILABILITY;
    @SerializedName("TOILETS_AVAILABILITY_BG")
    @Expose
    private String tOILETSAVAILABILITYBG;



    @SerializedName("GROUND_FLR_ROOMS_AVI")
    @Expose
    private String GROUND_FLR_ROOMS_AVI;
    @SerializedName("GROUND_CADIDATE_PER_ROOM")
    @Expose
    private String GROUND_CADIDATE_PER_ROOM;
    @SerializedName("OTHER_FLR_ROOMS_AVI")
    @Expose
    private String OTHER_FLR_ROOMS_AVI;
    @SerializedName("OTHER_CADIDATE_PER_ROOM")
    @Expose
    private String OTHER_CADIDATE_PER_ROOM;


    public String getiNSTITUTIONBIILDINGNAME() {
        return iNSTITUTIONBIILDINGNAME;
    }

    public void setiNSTITUTIONBIILDINGNAME(String iNSTITUTIONBIILDINGNAME) {
        this.iNSTITUTIONBIILDINGNAME = iNSTITUTIONBIILDINGNAME;
    }

    public String getiNSTITUTEID() {
        return iNSTITUTEID;
    }

    public void setiNSTITUTEID(String iNSTITUTEID) {
        this.iNSTITUTEID = iNSTITUTEID;
    }

    public String getnOOFCANDIDATES() {
        return nOOFCANDIDATES;
    }

    public void setnOOFCANDIDATES(String nOOFCANDIDATES) {
        this.nOOFCANDIDATES = nOOFCANDIDATES;
    }

    public String geteLECTRICITYAVAILABILITY() {
        return eLECTRICITYAVAILABILITY;
    }

    public void seteLECTRICITYAVAILABILITY(String eLECTRICITYAVAILABILITY) {
        this.eLECTRICITYAVAILABILITY = eLECTRICITYAVAILABILITY;
    }

    public String getfURNITUREAVAILABILITY() {
        return fURNITUREAVAILABILITY;
    }

    public void setfURNITUREAVAILABILITY(String fURNITUREAVAILABILITY) {
        this.fURNITUREAVAILABILITY = fURNITUREAVAILABILITY;
    }

    public String getdRINKINGWATERAVAILABILITY() {
        return dRINKINGWATERAVAILABILITY;
    }

    public void setdRINKINGWATERAVAILABILITY(String dRINKINGWATERAVAILABILITY) {
        this.dRINKINGWATERAVAILABILITY = dRINKINGWATERAVAILABILITY;
    }

    public String gettOILETSAVAILABILITYBG() {
        return tOILETSAVAILABILITYBG;
    }

    public void settOILETSAVAILABILITYBG(String tOILETSAVAILABILITYBG) {
        this.tOILETSAVAILABILITYBG = tOILETSAVAILABILITYBG;
    }

    public String getGROUND_FLR_ROOMS_AVI() {
        return GROUND_FLR_ROOMS_AVI;
    }

    public void setGROUND_FLR_ROOMS_AVI(String GROUND_FLR_ROOMS_AVI) {
        this.GROUND_FLR_ROOMS_AVI = GROUND_FLR_ROOMS_AVI;
    }

    public String getGROUND_CADIDATE_PER_ROOM() {
        return GROUND_CADIDATE_PER_ROOM;
    }

    public void setGROUND_CADIDATE_PER_ROOM(String GROUND_CADIDATE_PER_ROOM) {
        this.GROUND_CADIDATE_PER_ROOM = GROUND_CADIDATE_PER_ROOM;
    }

    public String getOTHER_FLR_ROOMS_AVI() {
        return OTHER_FLR_ROOMS_AVI;
    }

    public void setOTHER_FLR_ROOMS_AVI(String OTHER_FLR_ROOMS_AVI) {
        this.OTHER_FLR_ROOMS_AVI = OTHER_FLR_ROOMS_AVI;
    }

    public String getOTHER_CADIDATE_PER_ROOM() {
        return OTHER_CADIDATE_PER_ROOM;
    }

    public void setOTHER_CADIDATE_PER_ROOM(String OTHER_CADIDATE_PER_ROOM) {
        this.OTHER_CADIDATE_PER_ROOM = OTHER_CADIDATE_PER_ROOM;
    }
}

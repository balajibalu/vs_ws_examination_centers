package com.codetree.verification.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;


import com.codetree.verification.BuildConfig;
import com.codetree.verification.R;
import com.codetree.verification.models.Data;
import com.codetree.verification.models.LoginInput;
import com.codetree.verification.models.LoginResponse;
import com.codetree.verification.models.VersionModel;
import com.codetree.verification.utils.Constants;
import com.codetree.verification.utils.HFAUtils;
import com.codetree.verification.utils.PrefUtils;
import com.codetree.verification.utils.Preferences;
import com.codetree.verification.utils.SPSProgressDialog;
import com.codetree.verification.webservices.ApiCall;
import com.codetree.verification.webservices.RestAdapter;

import java.net.SocketTimeoutException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static final String LOG_TAG = LoginActivity.class.getSimpleName();
    LoginActivity mA;
    EditText etUserMobile, etPassword;
    String userMobile, userPassword;
    Spinner sp_login_level;
    String loginType="0";
// 9491035816  Mpdo@816 type 5
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mA = LoginActivity.this;
        etUserMobile = findViewById(R.id.etUserMobile);
        etPassword = findViewById(R.id.etPassword);
        sp_login_level = findViewById(R.id.sp_login_level);
        sp_login_level.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (sp_login_level.getSelectedItemPosition() == 0) {
                    loginType = "0";
                } else if (sp_login_level.getSelectedItemPosition() == 1) {
                    loginType = "1";
                }else if (sp_login_level.getSelectedItemPosition() == 2) {
                    loginType = "5";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        versionCheckService();
    }

    private void versionCheckService() {
        //HFAUtils.hideSoftKeyboard(activity);
        if (HFAUtils.isOnline(mA)) {
            SPSProgressDialog.showProgressDialog(mA);
            ApiCall apiService = RestAdapter.getClient().create(ApiCall.class);
            Call<VersionModel> call = apiService.checkversion(BuildConfig.VERSION_NAME);
            call.enqueue(new Callback<VersionModel>() {
                @Override
                public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                    try {
                        SPSProgressDialog.dismissProgressDialog();
                        if (!TextUtils.isEmpty(response.body().getStatus())
                                && response.body().getStatus().equalsIgnoreCase("Failure")) {
                            //callLoginService();
                            // moveNext();
                           // navigateToNextScreen();
                            openUpdateDialog();
                        } else {
                            HFAUtils.showToast(mA, "Valid Version");
                        }
                    } catch (Exception e) {
                        HFAUtils.showToast(mA, "Network /Server related Issue. Please Try Again Later.");
                    }
                }

                @Override
                public void onFailure(Call<VersionModel> call, Throwable t) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (t instanceof SocketTimeoutException) {
                        versionCheckService();
                    } else {
                        HFAUtils.showToast(mA, "Unable to check app_version.");
                        Log.e("failure", "failure");
                    }
                }
            });
        } else {
            HFAUtils.showToast(mA, "Please check Internet Connection.");
        }
    }
    private void openUpdateDialog() {
        new android.app.AlertDialog.Builder(this)
                .setMessage("You're using older version please update the app")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Preferences.getIns().clear();
                        ActivityCompat.finishAffinity(LoginActivity.this);
                    }
                })
                .show();
    }


    public void makeLogin(View view) {
        userMobile = etUserMobile.getText().toString();
        userPassword = etPassword.getText().toString();


        if (loginType.equalsIgnoreCase("0")) {
            HFAUtils.showToast(mA, "Please Select Login Type");
            return;
        }

        if (TextUtils.isEmpty(userMobile)) {
            HFAUtils.showToast(mA, "Please Enter Mobile Number");
            return;
        }
        if (TextUtils.isEmpty(userPassword)) {
            HFAUtils.showToast(mA, "Please Enter Password");
            return;
        }
        //startActivity(new Intent(this, MainActivity.class));

        if (loginType.equalsIgnoreCase("1")) {
            LoginInput loginInputModel = new LoginInput();
            loginInputModel.setMobileNumber(userMobile);
            loginInputModel.setPassword(userPassword);
            loginInputModel.setType(loginType);
            login(loginInputModel);
        } else if (loginType.equalsIgnoreCase("5")){
            LoginInput loginInputModel = new LoginInput();
            loginInputModel.setMobileNumber(userMobile);
            loginInputModel.setPassword(userPassword);
            loginInputModel.setType(loginType);
            mpdoLogin(loginInputModel);
        }
    }

    private void login(LoginInput gmLoginInput) {
        if (HFAUtils.isOnline(mA)) {
            SPSProgressDialog.showProgressDialog(mA);
            ApiCall apiCall = RestAdapter.createServiceWithAuth1(ApiCall.class);
            Call<LoginResponse> call = apiCall.getGmLogin(Constants.Username,Constants.Password,Constants.Session_Key,gmLoginInput);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (response != null && response.body() != null) {
                        Log.d(LOG_TAG, "getGmLogin() - onResponse :" + response.body());
                        LoginResponse successResponse = response.body();
                        if (successResponse.getStatus()==200) {
                            List<Data> data = successResponse.getData();
                            HFAUtils.showToast(mA, successResponse.getReason());
                            Preferences.getIns().setRememberMe(true);
                            Preferences.getIns().setMobileNo(etUserMobile.getText().toString());
                            Preferences.getIns().setDistrictCode(data.get(0).getDISTRICTCODE());
                            Preferences.getIns().setDistrictName(data.get(0).getDISTRICTNAME());
                            Preferences.getIns().setLoginToken(loginType);

                            String districtName = data.get(0).getDISTRICTNAME();
                            String districtCode = data.get(0).getDISTRICTCODE();
                            Intent intent = new Intent(mA, MainActivity.class);
                            intent.putExtra("district_code", districtCode);
                            intent.putExtra("district_name", districtName);
                            startActivity(intent);
                            finish();
                        } else if (successResponse.getStatus()==400 ) {
                            HFAUtils.showToast(mA, response.body().getReason());

                        } else {
                            HFAUtils.showToast(mA, successResponse.getReason());
                        }
                    } else {
                        Log.e(LOG_TAG, "getGmLogin() - onResponse : null response");
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    SPSProgressDialog.dismissProgressDialog();
                    Log.e(LOG_TAG, "getGmLogin() - onFailure :" + t.getMessage());
                    HFAUtils.showToast(mA, t.getMessage());
                }
            });
        }
    }



    private void mpdoLogin(LoginInput gmLoginInput) {
        if (HFAUtils.isOnline(mA)) {
            SPSProgressDialog.showProgressDialog(mA);
            Log.v("Response1",gmLoginInput.getType()+","+gmLoginInput.getMobileNumber()+","+gmLoginInput.getPassword());

            ApiCall apiCall = RestAdapter.createServiceWithAuth1(ApiCall.class);
            Call<LoginResponse> call = apiCall.getGmLogin(Constants.Username,Constants.Password,Constants.Session_Key,gmLoginInput);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (response != null && response.body() != null) {
                        Log.d(LOG_TAG, "getGmLogin() - onResponse :" + response.body().getStatus());
                        LoginResponse successResponse = response.body();

                        if (!TextUtils.isEmpty(String.valueOf(successResponse.getStatus()))) {
                           // HFAUtils.showToast(mA, "Test1123"+successResponse.getStatus());

                            if (successResponse.getStatus() == 200) {
                                List<Data> data = successResponse.getData();
                                Preferences.getIns().setMobileNo(etUserMobile.getText().toString());
                                Preferences.getIns().setLoginToken(loginType);
                                Preferences.getIns().setDistrictCode(data.get(0).getDISTRICTCODE());
                                Preferences.getIns().setDistrictName(data.get(0).getDISTRICTNAME());
                                Preferences.getIns().setMandalCode(data.get(0).getMANDALCODE());
                                Preferences.getIns().setMandalName(data.get(0).getMANDALNAME());
                                Preferences.getIns().setRoU(data.get(0).getRURALURBAN());
                                Preferences.getIns().setLoginToken(loginType);
                                HFAUtils.showToast(mA, successResponse.getReason());
                                String districtName = data.get(0).getDISTRICTNAME();
                                String districtCode = data.get(0).getDISTRICTCODE();
                                Preferences.getIns().setRememberMe(true);
                                Intent intent = new Intent(mA, MainActivity.class);
                                intent.putExtra("district_code", districtCode);
                                intent.putExtra("district_name", districtName);
                                startActivity(intent);
                                finish();
                            } else if (successResponse.getStatus() == 400) {
                                HFAUtils.showToast(mA, response.body().getReason());

                            } else {
                                HFAUtils.showToast(mA, successResponse.getReason());
                            }
                        }
                    } else {
                        Log.e(LOG_TAG, "getGmLogin() - onResponse : null response");
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    SPSProgressDialog.dismissProgressDialog();
                    Log.e(LOG_TAG, "getGmLogin() - onFailure :" + t.getMessage());
                    HFAUtils.showToast(mA, t.getMessage());
                }
            });
        }
    }


}

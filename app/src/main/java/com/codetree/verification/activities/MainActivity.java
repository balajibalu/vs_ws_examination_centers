package com.codetree.verification.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codetree.verification.BuildConfig;
import com.codetree.verification.R;
import com.codetree.verification.adapters.InstitutesAdapter;
import com.codetree.verification.adapters.LocalitiesAdapter;
import com.codetree.verification.adapters.MandalsAdapter;
import com.codetree.verification.adapters.VillagesAdapter;
import com.codetree.verification.models.InstituteInput;
import com.codetree.verification.models.SubmitData;
import com.codetree.verification.models.SubmitResponse;
import com.codetree.verification.models.VersionModel;
import com.codetree.verification.models.institute_details.Data;
import com.codetree.verification.models.institute_details.InstituteData;
import com.codetree.verification.models.localities.LocalitiesResponse;
import com.codetree.verification.models.mandals.MandalResponse;
import com.codetree.verification.models.mandals.MasterRequest;
import com.codetree.verification.models.mandals.Masterlist;
import com.codetree.verification.models.vilages.VillagesResponse;
import com.codetree.verification.utils.Constants;
import com.codetree.verification.utils.GPSTracker;
import com.codetree.verification.utils.HFAUtils;
import com.codetree.verification.utils.ImageUtils;
import com.codetree.verification.utils.PrefUtils;
import com.codetree.verification.utils.Preferences;
import com.codetree.verification.utils.RequestCameraObj;
import com.codetree.verification.utils.SPSProgressDialog;
import com.codetree.verification.utils.ZipprGPSService;
import com.codetree.verification.webservices.APIError;
import com.codetree.verification.webservices.ApiCall;
import com.codetree.verification.webservices.ErrorUtils;
import com.codetree.verification.webservices.RestAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codetree.verification.utils.ImageUtils.scaleBitmap;

public class MainActivity extends AppCompatActivity {
    /*Activity Instance*/
    Activity activity;
    /*Views Declaration*/
    ImageView geoTaggedImage;
    Dialog dialog;
    LinearLayout ll_user_details;
    TextView[] tvQuestions, tv_id, tv_mobile, mem_gpward, tv_attendece_status, mem_status;
    RadioGroup[] answers;
    RadioButton answerYes;
    RadioButton answerNo;
    Button btnSubmit;
    Dialog dg;
    private ListView lv_data;
    EditText tvInstitute;
    EditText edNoOfCandidatesGroundFloor, tvCategory, edNoOfCandidatesOtherFloor, edNoOfRoomsGroundFloor, edNoOfRoomsOtherFloor;
    TextView tv_totalCandidatesAcc;
    LinearLayout llLocality;
    LinearLayout llMandalLogin, llDistrictLogin;
    LinearLayout llMandal, llVillage;


    ListView listPopUp;
    /*Required Permissions Constants*/
    public static final String CAMERA = Manifest.permission.CAMERA;
    public static final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    public static final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final String READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE;
    public static final String ACCESS_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String ACCESS_COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    /*Required Permissions Arrays*/
    public static final String[] cameraParams = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE};
    public static final String[] locationParams = new String[]{READ_PHONE_STATE, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};

    /*Permission Request Constants*/
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 101;
    public static final int CAMERA_ACCESS = 10000;
    public static final int CAMERA_CAPTURE = 10001;
    public static final int CAMERA_REQUEST = 10002;
    public static final int LOCATION_ACCESS = 10003;
    public static final int LOCATION_REQUEST = 10004;
    public static final int APP_REQUEST = 10005;

    /*Permission Granted Constant*/
    public static final int GRANTED = PackageManager.PERMISSION_GRANTED;

    /**/
    boolean imgType = true;
    PrefUtils prefUtils;
    String imageBase64 = "";
    private IntentFilter mIntentFilter;
    String[] answerStrings;


    String latitude = "";
    String longitude = "";

    private static Uri imageUri;
    String imei = "";
    private List<Data> instituteList = new ArrayList<>();
    LinearLayout llData;
    RadioButton rbYes2, rbNo2, rbYes3, rbNo3, rbYes4, rbNo4, rbYes5, rbNo5, rbYes6, rbNo6, rbYes7, rbNo7;
    EditText tvDistrict, tvRorU, tvMandal, tvVillageOrWard, tvLocality, tvStreet, tvBuildingName, tvLandMark;


    String selectedDistrict, districtCode, selectedROrU = "", selectedMandal, selectedVillage, selectedLocality, street, buildingName, landMark;
    private List<Masterlist> mandalList = new ArrayList<>();
    private List<com.codetree.verification.models.vilages.Masterlist> villagesList = new ArrayList<>();
    private List<com.codetree.verification.models.localities.Masterlist> localitiesList = new ArrayList<>();
    private String selectedCenterCategory;
    TextView tvCenterCategory;
    private String selectedMandalCode = "";

    LinearLayout llCategory;
    private String selectedInstituteCode = "";
    private String villageCode = "";
    private String selectedLocalityCode = "";
    RadioGroup rg2, rg3, rg4, rg5, rg6, rg7;
    String LoginType = "";
    private String mandalCode;
    private EditText tvMandal2, tvRorU2;

    // Total Candidates=(GroundFloorTotalRooms * NoOfCandidatesPerRoomInGroundFloor) + (OtherFloorsTotalRooms * NoOfCandidatesPerRoomInOtherFloors)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        versionCheckService();

        ll_user_details = (LinearLayout) findViewById(R.id.llquestions);
        HFAUtils.hideKeyboard(this);
        btnSubmit = findViewById(R.id.btnSubmit);
        prefUtils = PrefUtils.getInstance(getApplicationContext());
        rbYes2 = findViewById(R.id.rb2Yes);
        rbNo2 = findViewById(R.id.rb2No);
        rbYes3 = findViewById(R.id.rb3Yes);
        rbNo3 = findViewById(R.id.rb3No);
        rbYes4 = findViewById(R.id.rb4Yes);
        rbNo4 = findViewById(R.id.rb4No);
        rbYes5 = findViewById(R.id.rb5Yes);
        rbNo5 = findViewById(R.id.rb5No);


        rbYes6 = findViewById(R.id.rb6Yes);
        rbNo6 = findViewById(R.id.rb6No);
        rbYes7 = findViewById(R.id.rb7Yes);
        rbNo7 = findViewById(R.id.rb7No);

        llMandal = findViewById(R.id.llMandal);
        llVillage = findViewById(R.id.llVillage);

        rg2 = findViewById(R.id.rg2);
        rg3 = findViewById(R.id.rg3);
        rg4 = findViewById(R.id.rg4);
        rg5 = findViewById(R.id.rg5);
        rg6 = findViewById(R.id.rg6);
        rg7 = findViewById(R.id.rg7);
        llData = findViewById(R.id.llData);
        llLocality = findViewById(R.id.llLocality);
        llCategory = findViewById(R.id.llCategory);
        tvCenterCategory = findViewById(R.id.tvCenterCategory);
        edNoOfCandidatesGroundFloor = findViewById(R.id.edNoOfCandidatesGroundFloor);
        edNoOfCandidatesOtherFloor = findViewById(R.id.edNoOfCandidatesOtherFloor);
        edNoOfRoomsGroundFloor = findViewById(R.id.edNoOfRoomsGroundFloor);
        edNoOfRoomsOtherFloor = findViewById(R.id.edNoOfRoomsOtherFloor);


        tv_totalCandidatesAcc = findViewById(R.id.tv_totalCandidatesAcc);

        tvCategory = findViewById(R.id.tvCategory);
        llMandalLogin = findViewById(R.id.llMandalLogin);
        llDistrictLogin = findViewById(R.id.llDistrictLogin);
        activity = this;
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ZipprGPSService.BROADCAST_ACTION);
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        tvInstitute = findViewById(R.id.tvInstitute);
        tvDistrict = findViewById(R.id.tvDistrict);
        tvRorU = findViewById(R.id.tvRorU);
        tvMandal = findViewById(R.id.tvMandal);
        tvMandal2 = findViewById(R.id.tvMandal2);
        tvRorU2 = findViewById(R.id.tvRorU2);
        tvVillageOrWard = findViewById(R.id.tvVillageOrWard);
        tvLocality = findViewById(R.id.tvLocality);
        tvStreet = findViewById(R.id.tvStreet);
        tvBuildingName = findViewById(R.id.tvBuildingName);
        tvLandMark = findViewById(R.id.tvLandMark);
        LoginType = Preferences.getIns().getLoginToken();


        edNoOfCandidatesGroundFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString().trim()) || edNoOfCandidatesGroundFloor.getText().toString().trim().length() != 0) {
                    if (TextUtils.isEmpty(edNoOfCandidatesOtherFloor.getText().toString().trim()) || edNoOfCandidatesOtherFloor.getText().toString().trim().length() == 0) {
                        if (TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText("0");
                        } else {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())));
                        }
                    } else {
                        if (TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim()) && !TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())));
                        } else if (!TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim()) && TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())));
                        } else if (!TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim()) && !TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf((Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())) + (Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim()))));
                        } else {
                            tv_totalCandidatesAcc.setText("0");
                        }


                    }
                } else {
                    if (TextUtils.isEmpty(edNoOfCandidatesOtherFloor.getText().toString().trim()) || edNoOfCandidatesOtherFloor.getText().toString().trim().length() == 0) {
                        tv_totalCandidatesAcc.setText("0");
                    } else {
                        if (!TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())));
                        } else {
                            tv_totalCandidatesAcc.setText("0");
                        }
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edNoOfCandidatesOtherFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!TextUtils.isEmpty(edNoOfCandidatesOtherFloor.getText().toString().trim()) || edNoOfCandidatesOtherFloor.getText().toString().trim().length() != 0) {
                    if (TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString().trim()) || edNoOfCandidatesGroundFloor.getText().toString().trim().length() == 0) {
                        if (TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText("0");
                        } else {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())));
                        }
                    } else {
                        if (TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim()) && !TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())));
                        } else if (!TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim()) && TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())));
                        } else if (!TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim()) && !TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf((Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())) + (Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim()))));
                        } else {
                            tv_totalCandidatesAcc.setText("0");
                        }


                    }
                } else {
                    if (TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString().trim()) || edNoOfCandidatesGroundFloor.getText().toString().trim().length() == 0) {
                        tv_totalCandidatesAcc.setText("0");
                    } else {
                        if (!TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())));
                        } else {
                            tv_totalCandidatesAcc.setText("0");
                        }
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edNoOfRoomsGroundFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString().trim()) || edNoOfCandidatesGroundFloor.getText().toString().trim().length() != 0) {
                    if (TextUtils.isEmpty(edNoOfCandidatesOtherFloor.getText().toString().trim()) || edNoOfCandidatesOtherFloor.getText().toString().trim().length() == 0) {
                        if (TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText("0");
                        } else {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())));
                        }
                    } else {
                        if (TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim()) && !TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())));
                        } else if (!TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim()) && TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())));
                        } else if (!TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim()) && !TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf((Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())) + (Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim()))));
                        } else {
                            tv_totalCandidatesAcc.setText("0");
                        }


                    }
                } else {
                    if (TextUtils.isEmpty(edNoOfCandidatesOtherFloor.getText().toString().trim()) || edNoOfCandidatesOtherFloor.getText().toString().trim().length() == 0) {
                        tv_totalCandidatesAcc.setText("0");
                    } else {
                        if (!TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())));
                        } else {
                            tv_totalCandidatesAcc.setText("0");
                        }
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edNoOfRoomsOtherFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!TextUtils.isEmpty(edNoOfCandidatesOtherFloor.getText().toString().trim()) || edNoOfCandidatesOtherFloor.getText().toString().trim().length() != 0) {
                    if (TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString().trim()) || edNoOfCandidatesGroundFloor.getText().toString().trim().length() == 0) {
                        if (TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText("0");
                        } else {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())));
                        }
                    } else {
                        if (TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim()) && !TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())));
                        } else if (!TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim()) && TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())));
                        } else if (!TextUtils.isEmpty(edNoOfRoomsOtherFloor.getText().toString().trim()) && !TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf((Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsOtherFloor.getText().toString().trim())) + (Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim()))));
                        } else {
                            tv_totalCandidatesAcc.setText("0");
                        }


                    }
                } else {
                    if (TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString().trim()) || edNoOfCandidatesGroundFloor.getText().toString().trim().length() == 0) {
                        tv_totalCandidatesAcc.setText("0");
                    } else {
                        if (!TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString().trim())) {
                            tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString().trim()) *
                                    Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString().trim())));
                        } else {
                            tv_totalCandidatesAcc.setText("0");
                        }
                    }
                }


            }


            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        // copy
       /* edNoOfCandidatesOtherFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!TextUtils.isEmpty(edNoOfCandidatesOtherFloor.getText().toString()) || edNoOfCandidatesOtherFloor.getText().toString().length()!=0)
                {
                    if (TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString()) || edNoOfCandidatesGroundFloor.getText().toString().length()==0 ){
                        tv_totalCandidatesAcc.setText(edNoOfCandidatesOtherFloor.getText().toString());
                    }else
                    {
                        tv_totalCandidatesAcc.setText(String.valueOf(Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString()) +
                                Integer.parseInt(edNoOfCandidatesOtherFloor.getText().toString())));

                    }
                }else
                {
                    if (TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString()) || edNoOfCandidatesGroundFloor.getText().toString().length()==0 ){
                        tv_totalCandidatesAcc.setText("0");
                    }else
                    {
                        tv_totalCandidatesAcc.setText(edNoOfCandidatesGroundFloor.getText().toString());

                    }
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/

      /* edNoOfCandidatesGroundFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(edNoOfRoomsGroundFloor.getText().toString())){
                    Toast.makeText(activity, "Please enter no. of ground floor rooms", Toast.LENGTH_SHORT).show();
                }else
                {
                   if (!TextUtils.isEmpty(edNoOfCandidatesGroundFloor.getText().toString())){
                       tv_totalCandidatesAcc.setText(String.valueOf
                               (Integer.parseInt(edNoOfRoomsGroundFloor.getText().toString()) *
                                Integer.parseInt(edNoOfCandidatesGroundFloor.getText().toString())));
                   }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
*/


        if (Preferences.getIns().getRorU().equalsIgnoreCase("R")) {
            tvRorU.setText("Rural");
            selectedROrU = Preferences.getIns().getRorU();
            llLocality.setVisibility(View.GONE);

        } else if (Preferences.getIns().getRorU().equalsIgnoreCase("U")) {
            tvRorU.setText("Urban");
            selectedROrU = Preferences.getIns().getRorU();
            llLocality.setVisibility(View.VISIBLE);


        } else if (Preferences.getIns().getRorU().equalsIgnoreCase("T")) {
            tvRorU.setText("Tribal");
            selectedROrU = Preferences.getIns().getRorU();
            llLocality.setVisibility(View.GONE);


        } else {
            tvRorU.setText(Preferences.getIns().getRorU());
            if (!LoginType.equals("1")) {
                tvRorU.setHint("");
            }
            selectedROrU = Preferences.getIns().getRorU();
            llLocality.setVisibility(View.GONE);


        }

        tvRorU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginType.equals("1")) {
                    if (tvInstitute.getText().toString().equalsIgnoreCase("")) {
                        HFAUtils.showToast(activity, "Please Select Institution or Center");
                    } else if (tvCenterCategory.getText().toString().equalsIgnoreCase("")) {
                        HFAUtils.showToast(activity, "Please Select Center of Category");
                    } else {
                        showDialogWithList(2);
                    }
                }
            }
        });
        if (!TextUtils.isEmpty(Preferences.getIns().getMandalCode()) && !TextUtils.isEmpty(Preferences.getIns().getMandaltName())) {
            tvVillageOrWard.setText("");
            tvStreet.setText("");
            tvBuildingName.setText("");
            tvLandMark.setText("");
            tvLocality.setText("");

            selectedMandalCode = Preferences.getIns().getMandalCode();
            tvMandal.setText(Preferences.getIns().getMandaltName());
            /*if (!TextUtils.isEmpty(selectedROrU)) {
                MasterRequest masterRequest = new MasterRequest();
                masterRequest.setDcode(districtCode);
                masterRequest.setRuflag(selectedROrU);
                masterRequest.setFtype("3");
                masterRequest.setMcode(selectedMandalCode);
                getVillage(masterRequest);
            }else
            {
                HFAUtils.showToast(activity, "Please Select Rural/Urban/Tribal");
            }
*/
        } else {
            selectedMandalCode = "0";
            tvMandal.setText(Preferences.getIns().getMandaltName());
            if (!LoginType.equals("1")) {
                tvMandal.setHint("");
            }
        }
        tvMandal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginType.equals("1")) {
                    if (tvRorU.getText().toString().equalsIgnoreCase("")) {
                        HFAUtils.showToast(activity, "Please Select Rural or Urban");
                    } else {
                        MasterRequest masterRequest = new MasterRequest();
                        masterRequest.setDcode(districtCode);
                        masterRequest.setRuflag(selectedROrU);
                        masterRequest.setFtype("2");
                        getMandals(masterRequest);

                    }
                }
            }
        });
        tvVillageOrWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvMandal.getText().toString().equalsIgnoreCase("")) {
                    HFAUtils.showToast(activity, "No Mandal/Municipality Found to Select Village");
                } else {
                    MasterRequest masterRequest = new MasterRequest();
                    masterRequest.setDcode(districtCode);
                    masterRequest.setRuflag(selectedROrU);
                    masterRequest.setFtype("3");
                    masterRequest.setMcode(selectedMandalCode);
                    getVillage(masterRequest);

                    //  showDialogWithList(4);
                }
            }
        });
        tvCenterCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvInstitute.getText().toString().equalsIgnoreCase("")) {
                    HFAUtils.showToast(activity, "Please Select Institution or Center");
                } else {
                    showDialogWithList(6);
                }
            }
        });
        tvLocality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvVillageOrWard.getText().toString().equalsIgnoreCase("")) {
                    HFAUtils.showToast(activity, "Please Select Village/Ward");
                } else {
                    if (localitiesList.size()>0)
                    showDialogWithList(5);
                    else
                        HFAUtils.showToast(activity, "No Locality found");

                }
            }
        });


        selectedDistrict = Preferences.getIns().getDistrictName();
        districtCode = Preferences.getIns().getDistrictCode();
        // mandalCode = Preferences.getIns().getMandalCode();
        // Toast.makeText(activity, "MandalCode"+mandalCode, Toast.LENGTH_SHORT).show();

        /*if (LoginType.equalsIgnoreCase("1")) {
            llDistrictLogin.setVisibility(View.VISIBLE);
            InstituteInput instituteInput = new InstituteInput();
            instituteInput.setDISTRICT_CODE(districtCode);
            instituteInput.setMANDAL_CODE("");
            instituteInput.setType("2");
            instituteInput.setRU_FLAG("");
            //getInstitutesList(instituteInput);
        } else {
            InstituteInput instituteInput = new InstituteInput();
            instituteInput.setDISTRICT_CODE(districtCode);
            instituteInput.setMANDAL_CODE(Preferences.getIns().getMandalCode());
            instituteInput.setType("2");
            instituteInput.setRU_FLAG(Preferences.getIns().getRorU());
            *//*getInstitutesList(instituteInput);
            llMandalLogin.setVisibility(View.VISIBLE);
            tvDistrict.setText(Preferences.getIns().getDistrictName());
            tvRorU2.setText(Preferences.getIns().getRorU());
            tvMandal2.setText(Preferences.getIns().getMandaltName());*//*
        }
*/


        tvDistrict.setText(selectedDistrict);
        tvInstitute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InstituteInput instituteInput = new InstituteInput();
                instituteInput.setDISTRICTCODE(districtCode);
                instituteInput.setRUFLAG("");
                instituteInput.setType("2");

                if (Preferences.getIns().getLoginToken().equals("1")) {
                    instituteInput.setMANDALCODE("");
                } else if (Preferences.getIns().getLoginToken().equals("5")) {
                    instituteInput.setMANDALCODE(Preferences.getIns().getMandalCode());
                    // Log.v("MandalCode",Preferences.getIns().getMandalCode());
                } else {
                    instituteInput.setMANDALCODE("");
                }

                getInstitutesList(instituteInput);

               /* if (instituteList.size() > 0) {
                    showDialogWithList(1);
                }*/
            }
        });
        // getInstitutesList();
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
        if (imei.isEmpty()) {
            readPhoneStatePermission();
        }
        if (!isMyServiceRunning(ZipprGPSService.class)) {
            startService(new Intent(this, ZipprGPSService.class));
        }
        getPresentLocation();
        geoTaggedImage = findViewById(R.id.geoTaggedImage);
        geoTaggedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked on Camera");
                if (HFAUtils.isOnline(activity)) {
                    final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        System.out.println("No GPS found");
                        buildAlertMessageNoGps();
                    } else {
                        System.out.println("Got GPS");
                        if (Constants.hadPhotoStore) {
                            if (Constants.showPhotoStore) {
                                showImageType();
                            } else {
                                openCamera();
                            }
                        } else {
                            checkCameraPermissions();
                        }
                    }
                } else {
                    HFAUtils.showToast(MainActivity.this, "No Internet");
                    System.out.println("Got GPS");
                    if (Constants.hadPhotoStore) {
                        if (Constants.showPhotoStore) {
                            showImageType();
                        } else {
                            openCamera();
                        }
                    } else {
                        checkCameraPermissions();
                    }
                }
            }
        });
        //  createMemberTable();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation();
            }
        });
    }

    private void checkValidation() {
        // Log.d("Rural_or_Urban", tvRorU.getText().toString());
        if (TextUtils.isEmpty(tvDistrict.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvDistrict.getHint());
        } else if (TextUtils.isEmpty(tvInstitute.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvInstitute.getHint());
        } else if (TextUtils.isEmpty(tvCenterCategory.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvCenterCategory.getHint());
        } else if (llDistrictLogin.getVisibility() == View.VISIBLE && TextUtils.isEmpty(tvRorU.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvRorU.getHint());
        } else if (llDistrictLogin.getVisibility() == View.VISIBLE && TextUtils.isEmpty(tvMandal.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvMandal.getHint());
        } else if (llVillage.getVisibility() == View.VISIBLE && TextUtils.isEmpty(tvVillageOrWard.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvVillageOrWard.getHint());
        } else if (TextUtils.isEmpty(tvStreet.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvStreet.getHint());
        } else if (TextUtils.isEmpty(tvBuildingName.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvBuildingName.getHint());
        } else if (TextUtils.isEmpty(tvLandMark.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvLandMark.getHint());
        } else if (tv_totalCandidatesAcc.getText().toString().isEmpty() || tv_totalCandidatesAcc.getText().toString().equals("0")) {
            HFAUtils.showToast(this, "Please Answer Question No.1");
        } else if (rg2.getCheckedRadioButtonId() == -1) {
            HFAUtils.showToast(this, "Please Answer Question No.2");
        } else if (rg3.getCheckedRadioButtonId() == -1) {
            HFAUtils.showToast(this, "Please Answer Question No.3");
        } else if (rg4.getCheckedRadioButtonId() == -1) {
            HFAUtils.showToast(this, "Please Answer Question No.4");
        } else if (rg5.getCheckedRadioButtonId() == -1) {
            HFAUtils.showToast(this, "Please Answer Question No.5");
        } else if (rg6.getCheckedRadioButtonId() == -1) {
            HFAUtils.showToast(this, "Please Answer Question No.6");
        } else if (rg7.getCheckedRadioButtonId() == -1) {
            HFAUtils.showToast(this, "Please Answer Question No.7");
        } else if (imageBase64.equalsIgnoreCase("")) {
            HFAUtils.showToast(this, "Please capture image.");
        } else if (llLocality.getVisibility() == View.VISIBLE && TextUtils.isEmpty(tvLocality.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvLocality.getHint());
        } else if (llCategory.getVisibility() == View.VISIBLE && TextUtils.isEmpty(tvCategory.getText().toString())) {
            HFAUtils.showToast(this, "Please " + tvCategory.getHint());
        } else {
            versionCheckService1();
        }
    }

    private void getPresentLocation() {
        //ZipprGPSService zipprGPSService = new ZipprGPSService();
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            double latitude = gpsTracker.getLatitude();
            prefUtils.saveString("Latitude", latitude + "");
            double longitude = gpsTracker.getLongitude();
            prefUtils.saveString("Longitude", longitude + "");
        } else {
            gpsTracker.showSettingsAlert(this);
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("serviceClass.getName()........"
                        + serviceClass.getName());
                return true;
            }
        }
        return false;
    }

    private void readPhoneStatePermission() {
        if (ActivityCompat.checkSelfPermission(activity
                , android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity
                , android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity
                , android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{android.Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            imei = HFAUtils.getIMEI(activity);
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS is disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void showImageType() {
        String[] imageType = {"Camera", "Photo Store"};
        System.out.println("Show image type clicked");
        try {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.view_selection);
            TextView tv_header = (TextView) dialog.findViewById(R.id.tvSelectionHeader);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            final EditText et_search = (EditText) dialog.findViewById(R.id.etSearch);
            et_search.setVisibility(View.GONE);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            listPopUp = (ListView) dialog.findViewById(R.id.listSelection);
            tv_header.setText("Select");
            ArrayAdapter<String> adp = new ArrayAdapter<String>(this, R.layout.view_list,
                    R.id.tvListAdapter, imageType);
            listPopUp.setAdapter(adp);
            listPopUp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (parent == listPopUp) {
                        String type = parent.getItemAtPosition(position).toString();
                        System.out.println("type: " + type);
                        if (type.equalsIgnoreCase("Camera")) {
                            openCamera();
                        } else {
                            //getImageData(-1, "");
                        }
                        dialog.cancel();
                    }
                }
            });
            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    private void openCamera() {
        if (HFAUtils.isOnline(activity)) {
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                System.out.println("No GPS found");
                buildAlertMessageNoGps();
            } else {
                System.out.println("Got GPS");
                checkCameraPermissions();
            }
        } else {
            HFAUtils.showToast(activity, "No Internet");
            checkCameraPermissions();
        }
        imgType = true;
    }

    private void checkCameraPermissions() {
        System.out.println("checkCameraPermissions");
        if (cameraPermissions(this)) {
            System.out.println("Inside");
            captureImage();
        } else {
            System.out.println("Outside");
        }
    }

    public static boolean cameraPermissions(Activity activity) {
        if (checkPermission(activity, CAMERA)
                || checkPermission(activity, READ_EXTERNAL_STORAGE)
                || checkPermission(activity, WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(activity, cameraParams, CAMERA_ACCESS);
            return false;
        }
        return true;
    }

    private static boolean checkPermission(Activity activity, String permission) {
        return ActivityCompat.checkSelfPermission(activity, permission) != GRANTED;
    }

    private static void requestPermissions(Activity activity, String[] params, int constant) {
        ActivityCompat.requestPermissions(activity, params, constant);
    }

    private void captureImage() {
        System.out.println("01");
        setImageUri(getOutputMediaFileUri(activity));
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        System.out.println("02");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, CAMERA_CAPTURE);
        System.out.println("03");
    }

    public static void setImageUri(Uri imageUri1) {
        imageUri = imageUri1;
    }

    public static Uri getImageUri() {
        return imageUri;
    }

    public static Uri getOutputMediaFileUri(Context context) {
        Uri photoURI = null;
        try {
            photoURI = FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider",
                    createImageFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photoURI;
    }

    public static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    public static boolean locationPermissions(Activity activity) {
        if (checkPermission(activity, READ_PHONE_STATE)
                || checkPermission(activity, ACCESS_FINE_LOCATION)
                || checkPermission(activity, ACCESS_COARSE_LOCATION)) {
            requestPermissions(activity, locationParams, LOCATION_ACCESS);
            return false;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK && (requestCode == CAMERA_CAPTURE)) {
                final Uri imageUri = getImageUri();
                if (imageUri != null) {
                    getContentResolver().notifyChange(imageUri, null);
                    ContentResolver cr = MainActivity.this.getContentResolver();
                    Bitmap bitmap;
                    try {
                        latitude = prefUtils.getString("Latitude");
                        longitude = prefUtils.getString("Longitude");
                        bitmap = MediaStore.Images.Media.getBitmap(cr, imageUri);
                        String capLatitude = "Latitude: " + latitude;
                        String capLongitude = "Longitude: " + longitude;
                        Bitmap scaledBitmap = scaleBitmap(bitmap);
                        Bitmap scaledBitmap1 = ImageUtils.processingBitmap(scaledBitmap, capLatitude, capLongitude);

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        scaledBitmap1.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
                        geoTaggedImage.setImageBitmap(scaledBitmap1);

                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        imageBase64 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                        RequestCameraObj requestCameraObj = new RequestCameraObj();
                        requestCameraObj.setBitmap(scaledBitmap1);
                        requestCameraObj.setImageUri(imageUri);
                        requestCameraObj.setBasephoto(imageBase64);
                        //capturedImagesList.add(requestCameraObj);
                        imgType = true;
                    } catch (Exception e) {
                        HFAUtils.showToast(MainActivity.this, "Failed to load");
                        Log.d("Batch Selection", "Failed to load", e);
                    }
                } else {
                    clearAllFields();
                    HFAUtils.showToast(MainActivity.this, "No Image Captured Please try again.");
                }
                //getImageFromCamera(imageUri);
            } else {
                Log.e("ENUM", "Not able to get Image");
            }
        } catch (Exception e) {
            Log.e("ENUM", e.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == GRANTED) {
                    getPresentLocation();
                } else {
                    HFAUtils.showToast(activity, getResources().getString(R.string.grant_location_permission));
                }
                break;
            case CAMERA_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == GRANTED) {
                    captureImage();
                } else {
                    HFAUtils.showToast(activity, getResources().getString(R.string.grant_camera_permissions));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.registerReceiver(broadcastReceiver, mIntentFilter);
        getPresentLocation();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String action = intent.getAction();
                if (action.trim().equalsIgnoreCase(ZipprGPSService.BROADCAST_ACTION)) {
                    Bundle b = intent.getExtras();
                    String lat = b.getString("Lat");
                    String lon = b.getString("Lon");
                    String accuracy = b.getString("Accuracy");
                    prefUtils.saveString("Latitude", lat);
                    prefUtils.saveString("Longitude", lon);
                    latitude = lat;
                    longitude = lon;
                    System.out.println("Latitude: " + lat);
                    System.out.println("Longitude: " + lon);
                    if (accuracy.length() == 0) {
                        accuracy = "0";
                    }
                    if (Double.parseDouble(accuracy) <= 20) {
                        activity.stopService(new Intent(activity, ZipprGPSService.class));
                    }
                }
            } catch (Exception e) {
            }
        }
    };

    private void createMemberTable() {
        int sizeofList = 5;
        ll_user_details.removeAllViews();
        tvQuestions = new TextView[sizeofList];
        answers = new RadioGroup[sizeofList];
        answerStrings = new String[sizeofList];

        for (int i = 0; i < sizeofList; i++) {
            View v = this.getLayoutInflater().inflate(R.layout.questions_item, null);

            TextView mem_sno;
            final int finalI = i;

            tvQuestions[i] = (TextView) v.findViewById(R.id.tvQuestion);
            answers[i] = v.findViewById(R.id.rgAnswer);

//            mem_sno.setText("" + (i + 1));

          /*  if (memberData.get(i).getVV_ID() != null && memberData.get(i).getVV_ID() != "") {
                tv_id[i].setText(memberData.get(i).getVV_ID());
                tv_id[i].setPaintFlags(tv_id[i].getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            }*/

            final int finalI1 = i;
            answers[i].setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.rbYes:
                            answerStrings[finalI1] = "1";
                            break;
                        case R.id.rbNo:
                            answerStrings[finalI1] = "0";
                            break;
                    }
                }
            });
            ll_user_details.addView(v);
        }
    }

    private void showDialogWithList(final int index) {

        System.out.println("Entered Index is...." + index);
        try {
            System.out.print("Entered try..........");
            dg = new Dialog(activity);
            dg.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dg.setContentView(R.layout.dialog_with_list);
            TextView tv_header = (TextView) dg
                    .findViewById(R.id.tv_selecion_header);
            dg.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            final EditText et_search = (EditText) dg.findViewById(R.id.et_search);
            et_search.setVisibility(View.GONE);
            lv_data = (ListView) dg.findViewById(R.id.list_selection);
            if (index == 1) {
                tv_header.setText("Select Institute Name");
                List<String> instituteName = new ArrayList<>();
                for (int i = 0; i < instituteList.size(); i++) {
                    instituteName.add(instituteList.get(i).getiNSTITUTIONBIILDINGNAME());
                }/*
                ArrayAdapter<String> adp = new ArrayAdapter<String>(
                        activity, R.layout.dialog_with_list_item,
                        R.id.tv_list_adapetr, instituteName);*/
                final InstitutesAdapter adp = new InstitutesAdapter(MainActivity.this, instituteList);

                lv_data.setAdapter(adp);
                lv_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (parent == lv_data) {
                            String selectedPosition = parent.getItemAtPosition(position).toString();
                            String selectedInstituteName = instituteList.get(position).getiNSTITUTIONBIILDINGNAME();
                            selectedInstituteCode = instituteList.get(position).getiNSTITUTEID();
                            tvInstitute.setText(selectedInstituteName);
                            //  Log.d("Selected_Position", String.valueOf(position));
                            // llData.setVisibility(View.VISIBLE);
                            String electricityAvailability = instituteList.get(position).geteLECTRICITYAVAILABILITY();
                            String furniture_availability = instituteList.get(position).getfURNITUREAVAILABILITY();
                            String drinking_water_availability = instituteList.get(position).getdRINKINGWATERAVAILABILITY();
                            String toilets_availability_bg = instituteList.get(position).gettOILETSAVAILABILITYBG();
                            if (!TextUtils.isEmpty(instituteList.get(position).getGROUND_FLR_ROOMS_AVI())) {
                                edNoOfRoomsGroundFloor.setText(instituteList.get(position).getGROUND_FLR_ROOMS_AVI());
                            } else {
                                edNoOfRoomsGroundFloor.setText("0");
                            }

                            if (!TextUtils.isEmpty(instituteList.get(position).getGROUND_CADIDATE_PER_ROOM())) {
                                edNoOfCandidatesGroundFloor.setText(instituteList.get(position).getGROUND_CADIDATE_PER_ROOM());
                            } else {
                                edNoOfCandidatesGroundFloor.setText("0");
                            }


                            if (!TextUtils.isEmpty(instituteList.get(position).getOTHER_FLR_ROOMS_AVI())) {
                                edNoOfRoomsOtherFloor.setText(instituteList.get(position).getOTHER_FLR_ROOMS_AVI());
                            } else {
                                edNoOfRoomsOtherFloor.setText("0");
                            }


                            if (!TextUtils.isEmpty(instituteList.get(position).getOTHER_CADIDATE_PER_ROOM())) {
                                edNoOfCandidatesOtherFloor.setText(instituteList.get(position).getOTHER_CADIDATE_PER_ROOM());
                            } else {
                                edNoOfCandidatesOtherFloor.setText("0");
                            }


                            if (!TextUtils.isEmpty(instituteList.get(position).getOTHER_FLR_ROOMS_AVI()) || !TextUtils.isEmpty(instituteList.get(position).getGROUND_FLR_ROOMS_AVI())) {
                                tv_totalCandidatesAcc.setText(instituteList.get(position).getnOOFCANDIDATES());
                            } else {
                                tv_totalCandidatesAcc.setText("0");
                            }


                            if (electricityAvailability.equalsIgnoreCase("YES")) {
                                rbYes2.setChecked(true);
                            }
                            if (electricityAvailability.equalsIgnoreCase("NO")) {
                                rbNo2.setChecked(true);
                            }
                            if (furniture_availability.equalsIgnoreCase("YES")) {
                                rbYes3.setChecked(true);
                            }
                            if (furniture_availability.equalsIgnoreCase("NO")) {
                                rbNo3.setChecked(true);
                            }
                            if (drinking_water_availability.equalsIgnoreCase("YES")) {
                                rbYes4.setChecked(true);
                            }
                            if (drinking_water_availability.equalsIgnoreCase("NO")) {
                                rbNo4.setChecked(true);
                            }
                            if (toilets_availability_bg.equalsIgnoreCase("YES")) {
                                rbYes5.setChecked(true);
                            }
                            if (toilets_availability_bg.equalsIgnoreCase("NO")) {
                                rbNo5.setChecked(true);
                            }
                           /* if (LoginType.equalsIgnoreCase("5")) {
                                MasterRequest masterRequest = new MasterRequest();
                                masterRequest.setDcode(Preferences.getIns().getDistrictCode());
                                masterRequest.setRuflag(Preferences.getIns().getRorU());
                                masterRequest.setFtype("3");
                                masterRequest.setMcode(Preferences.getIns().getMandalCode());
                                getVillage(masterRequest);
                            } else {*/
                            /*}*/

                            System.out.print("\n" + "Selection of Position......................" + selectedPosition);
                            dg.cancel();

                            tvCenterCategory.setText("");
                            // tvRorU.setText("");
                            //tvMandal.setText("");
                            //tvVillageOrWard.setText("");
                            tvStreet.setText("");
                            tvBuildingName.setText("");
                            tvLandMark.setText("");
                            tvLocality.setText("");
                            tvCategory.setText("");
                        }
                    }
                });
                et_search.setVisibility(View.VISIBLE);
                et_search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        adp.filter(charSequence.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });
                HFAUtils.hideKeyboard(activity);

            } else if (index == 2) {
                tv_header.setText("Select Rural / Urban / Tribal");
                List<String> ruralOrUrban = new ArrayList<>();
                ruralOrUrban.add("Rural");
                ruralOrUrban.add("Urban");
                ruralOrUrban.add("Tribal");
                ArrayAdapter<String> adp = new ArrayAdapter<String>(
                        activity, R.layout.dialog_with_list_item,
                        R.id.tv_list_adapetr, ruralOrUrban);
                lv_data.setAdapter(adp);
                lv_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (parent == lv_data) {
                            String selectedPosition = parent.getItemAtPosition(position).toString();
                            selectedROrU = parent.getItemAtPosition(position).toString();
                            tvRorU.setText(selectedROrU);
                            Log.d("Selected_Position", String.valueOf(position));
                            if (selectedROrU.equalsIgnoreCase("Rural")) {
                                selectedROrU = "R";
                                mandalList.clear();
                                llLocality.setVisibility(View.GONE);
                            } else if (selectedROrU.equalsIgnoreCase("Urban")) {
                                selectedROrU = "U";
                                mandalList.clear();
                                llLocality.setVisibility(View.VISIBLE);
                            } else {
                                selectedROrU = "T";
                                mandalList.clear();
                                llLocality.setVisibility(View.GONE);
                            }
                           /* MasterRequest masterRequest = new MasterRequest();
                            masterRequest.setDcode(districtCode);
                            masterRequest.setRuflag(selectedROrU);
                            masterRequest.setFtype("2");
                            getMandals(masterRequest);
*/
                            tvMandal.setText("");
                            tvVillageOrWard.setText("");
                            tvStreet.setText("");
                            tvBuildingName.setText("");
                            tvLandMark.setText("");
                            tvLocality.setText("");
                            System.out.print("\n" + "Selection of Position......................" + selectedPosition);
                            dg.cancel();
                        }
                    }
                });

            } else if (index == 3) {
                tv_header.setText("Select Mandal/Muncipality");
                List<String> mandalNames = new ArrayList<>();
                for (int i = 0; i < mandalList.size(); i++) {
                    mandalNames.add(mandalList.get(i).getMANDALNAME());
                }
               /* ArrayAdapter<String> adp = new ArrayAdapter<String>(
                        activity, R.layout.dialog_with_list_item,
                        R.id.tv_list_adapetr, mandalNames);
               */
                final MandalsAdapter adp = new MandalsAdapter(MainActivity.this, mandalList);
                lv_data.setAdapter(adp);
                lv_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (parent == lv_data) {
                            String selectedPosition = parent.getItemAtPosition(position).toString();
                            selectedMandal = mandalList.get(position).getMANDALNAME();
                            selectedMandalCode = mandalList.get(position).getLGDMANDALCODE();
                            tvMandal.setText(selectedMandal);
                           /* MasterRequest masterRequest = new MasterRequest();
                            masterRequest.setDcode(districtCode);
                            masterRequest.setRuflag(selectedROrU);
                            masterRequest.setFtype("3");
                            masterRequest.setMcode(selectedMandalCode);
                            getVillage(masterRequest);
                           */
                            tvVillageOrWard.setText("");
                            tvStreet.setText("");
                            tvBuildingName.setText("");
                            tvLandMark.setText("");
                            tvLocality.setText("");
                            Log.d("Selected_Position", String.valueOf(position));
                            System.out.print("\n" + "Selection of Position......................" + selectedPosition);
                            dg.cancel();
                        }
                    }
                });
                et_search.setVisibility(View.VISIBLE);
                et_search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        adp.filter(charSequence.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });
                HFAUtils.hideKeyboard(activity);


            } else if (index == 4) {
                tv_header.setText("Select Village/Ward");
                final List<String> villagesNames = new ArrayList<>();
                for (int i = 0; i < villagesList.size(); i++) {
                    villagesNames.add(villagesList.get(i).getPANCHAYATNAME());
                }
               /* ArrayAdapter<String> adp = new ArrayAdapter<String>(
                        activity, R.layout.dialog_with_list_item,
                        R.id.tv_list_adapetr, villagesNames);*/
                final VillagesAdapter adp = new VillagesAdapter(MainActivity.this, villagesList);

                lv_data.setAdapter(adp);

                lv_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (parent == lv_data) {
                            String selectedPosition = parent.getItemAtPosition(position).toString();
                            selectedVillage = villagesList.get(position).getPANCHAYATNAME();
                            villageCode = villagesList.get(position).getLGDPANCHAYATCODE();
                            tvVillageOrWard.setText(selectedVillage);
                            /*if (LoginType.equalsIgnoreCase("1")) {*/
                            if (selectedROrU.equalsIgnoreCase("U")) {
                                MasterRequest masterRequest = new MasterRequest();
                                masterRequest.setDcode(districtCode);
                                masterRequest.setRuflag(selectedROrU);
                                masterRequest.setMcode(selectedMandalCode);
                                masterRequest.setFtype("4");
                                masterRequest.setVcode(villageCode);
                                getLocalities(masterRequest);
                            }
                           /* } else {
                                if (Preferences.getIns().getRorU().equalsIgnoreCase("U")){
                                    MasterRequest masterRequest = new MasterRequest();
                                    masterRequest.setDcode(Preferences.getIns().getDistrictCode());
                                    masterRequest.setRuflag(Preferences.getIns().getRorU());
                                    masterRequest.setMcode(Preferences.getIns().getMandalCode());
                                    masterRequest.setFtype("4");
                                    masterRequest.setVcode(villageCode);
                                    getLocalities(masterRequest);
                                }
                            }
*/
                            Log.d("Selected_Position", String.valueOf(position));
                            tvStreet.setText("");
                            tvBuildingName.setText("");
                            tvLandMark.setText("");
                            tvLocality.setText("");
                            System.out.print("\n" + "Selection of Position......................" + selectedPosition);
                            dg.cancel();

                        }
                    }
                });
                et_search.setVisibility(View.VISIBLE);
                et_search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        adp.filter(charSequence.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });
                HFAUtils.hideKeyboard(activity);

            } else if (index == 5) {
                tv_header.setText("Select Locality");
                final List<String> localityNames = new ArrayList<>();
                for (int i = 0; i < localitiesList.size(); i++) {
                    localityNames.add(localitiesList.get(i).getLOCALITYNAME());
                }
               /* ArrayAdapter<String> adp = new ArrayAdapter<String>(
                        activity, R.layout.dialog_with_list_item,
                        R.id.tv_list_adapetr, localityNames);
               */
                final LocalitiesAdapter adp = new LocalitiesAdapter(MainActivity.this, localitiesList);

                lv_data.setAdapter(adp);

                lv_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (parent == lv_data) {
                            String selectedPosition = parent.getItemAtPosition(position).toString();
                            selectedLocality = localitiesList.get(position).getLOCALITYNAME();
                            selectedLocalityCode = localitiesList.get(position).getLOCALITYCODE();
                            tvLocality.setText(selectedLocality);
                            //getLocalities();
                            Log.d("Selected_Position", String.valueOf(position));
                            tvStreet.setText("");
                            tvBuildingName.setText("");
                            tvLandMark.setText("");
                            System.out.print("\n" + "Selection of Position......................" + selectedPosition);
                            dg.cancel();
                        }
                    }
                });
                et_search.setVisibility(View.VISIBLE);
                et_search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        adp.filter(charSequence.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });
                HFAUtils.hideKeyboard(activity);

            } else if (index == 6) {
                tv_header.setText("Select Center Category");
                List<String> categories = new ArrayList<>();
                categories.add("District Headquarter");
                categories.add("Divisional Headquarter");
                categories.add("Other Municipal Headquarter");
                categories.add("Taluk Headquarter other than municipalities");
                categories.add("Any Other");
                ArrayAdapter<String> adp = new ArrayAdapter<String>(
                        activity, R.layout.dialog_with_list_item,
                        R.id.tv_list_adapetr, categories);
                lv_data.setAdapter(adp);
                lv_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (parent == lv_data) {
                            String selectedPosition = parent.getItemAtPosition(position).toString();
                            selectedCenterCategory = parent.getItemAtPosition(position).toString();
                            tvCenterCategory.setText(selectedCenterCategory);
                            if (selectedCenterCategory.equalsIgnoreCase("Any Other")) {
                                llCategory.setVisibility(View.VISIBLE);
                            } else {
                                llCategory.setVisibility(View.GONE);
                            }

                            Log.d("Selected_Position", String.valueOf(position));
                            // getVillage();
                            System.out.print("\n" + "Selection of Position......................" + selectedPosition);
                            dg.cancel();

                        }
                    }
                });
            }
            dg.setCancelable(true);
            dg.show();
        } catch (Exception e) {
            System.out.print("Exception.........." + e.getMessage());
        }
    }


    private void getInstitutesList(final InstituteInput instituteInput) {
        if (HFAUtils.isOnline(activity)) {
            SPSProgressDialog.showProgressDialog(activity);
            ApiCall apiService = RestAdapter.createServiceWithAuth1(ApiCall.class);

            Call<InstituteData> call = apiService.getInstituteDetails(Constants.Username,Constants.Password,Constants.Session_Key,instituteInput);
            call.enqueue(new Callback<InstituteData>() {
                @Override
                public void onResponse(Call<InstituteData> call, Response<InstituteData> response) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.d("onResponse() - Response", response.body().toString());
                        if (!TextUtils.isEmpty(response.body().getStatus()) && response.body().getStatus().equalsIgnoreCase("200")) {
                            HFAUtils.showToast(activity, response.body().getReason());
                            instituteList.clear();
                            instituteList = response.body().getData();
                            if (instituteList.size() > 0) {
                                showDialogWithList(1);
                            } else {
                                HFAUtils.showToast(activity, "No Institution Or centers Found");

                            }
                            // Log.d("Institutes_Length", String.valueOf(instituteList.size()));
                        } else {
                            instituteList.clear();
                            // Log.d("Institutes_Length", String.valueOf(instituteList.size()));
                            HFAUtils.showToast(activity, response.body().getReason());
                        }
                    } else {
                        APIError error = ErrorUtils.parseError(response);
                        Log.d("Error_Message", error.getMessage());
                        HFAUtils.showToast(activity, error.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<InstituteData> call, Throwable t) {
                    SPSProgressDialog.dismissProgressDialog();

                    if (t instanceof SocketTimeoutException) {
                        getInstitutesList(instituteInput);
                    } else {
                        HFAUtils.showToast(activity, "Please Retry");
                        Log.e("onFailure() - Response", t.getMessage());
                    }
                }
            });
        } else {
            HFAUtils.showToast(activity, "Please check Internet Connection.");
        }
    }

    private void getMandals(final MasterRequest masterRequest) {
        if (HFAUtils.isOnline(activity)) {
            SPSProgressDialog.showProgressDialog(activity);
            ApiCall apiService = RestAdapter.createServiceWithAuth1(ApiCall.class);
            Call<MandalResponse> call = apiService.getMandals(Constants.Username,Constants.Password,Constants.Session_Key,masterRequest);
            call.enqueue(new Callback<MandalResponse>() {
                @Override
                public void onResponse(Call<MandalResponse> call, Response<MandalResponse> response) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.d("onResponse() - Response", response.body().toString());
                        if (!TextUtils.isEmpty(response.body().getStatus()) && response.body().getStatus().equalsIgnoreCase("Success")) {
                            // HFAUtils.showToast(activity, response.body().getStatus());
                            mandalList.clear();
                            mandalList = response.body().getMasterlist();
                            if (mandalList.size() > 0) {
                                showDialogWithList(3);
                            } else {
                                HFAUtils.showToast(activity, "No Mandals/Municipality Found");

                            }
                            // Log.d("Mandals_Length", String.valueOf(mandalList.size()));
                        } else {
                            HFAUtils.showToast(activity, response.body().getReason());
                        }
                    } else {
                        APIError error = ErrorUtils.parseError(response);
                        Log.d("Error_Message", error.getMessage());
                        HFAUtils.showToast(activity, error.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<MandalResponse> call, Throwable t) {
                    if (t instanceof SocketTimeoutException) {
                        getMandals(masterRequest);
                    } else {
                        HFAUtils.showToast(activity, "Please Retry");
                        SPSProgressDialog.dismissProgressDialog();
                        Log.e("onFailure() - Response", t.getMessage());
                    }
                }
            });
        } else {
            HFAUtils.showToast(activity, "Please check Internet Connection.");
        }
    }

    private void getVillage(final MasterRequest masterRequest) {
        if (HFAUtils.isOnline(activity)) {
            SPSProgressDialog.showProgressDialog(activity);
            ApiCall apiService = RestAdapter.createServiceWithAuth1(ApiCall.class);
            Call<VillagesResponse> call = apiService.getVillages(Constants.Username,Constants.Password,Constants.Session_Key,masterRequest);
            call.enqueue(new Callback<VillagesResponse>() {
                @Override
                public void onResponse(Call<VillagesResponse> call, Response<VillagesResponse> response) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        if (response != null) {
                            Log.d("onResponse() - Response", response.body().toString());
                            if (!TextUtils.isEmpty(response.body().getStatus()) && response.body().getStatus().equalsIgnoreCase("Success")) {
                                //  HFAUtils.showToast(activity, response.body().getStatus());
                                villagesList.clear();
                                villagesList = response.body().getMasterlist();
                                if (villagesList.size() > 0) {
                                    showDialogWithList(4);
                                } else {
                                    HFAUtils.showToast(activity, "No Villages / Ward Found");

                                }
                                // Log.d("Villages_Length", String.valueOf(villagesList.size()));
                            } else {
                                HFAUtils.showToast(activity, response.body().getReason());
                            }
                        } else {
                            HFAUtils.showToast(activity, "No Data Found");
                        }

                    } else {
                        APIError error = ErrorUtils.parseError(response);
                        Log.d("Error_Message", error.getMessage());
                        HFAUtils.showToast(activity, error.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<VillagesResponse> call, Throwable t) {
                    if (t instanceof SocketTimeoutException) {
                        getVillage(masterRequest);
                    } else {
                        HFAUtils.showToast(activity, "Please Retry");
                        SPSProgressDialog.dismissProgressDialog();
                        Log.e("onFailure() - Response", t.getMessage());
                    }
                }
            });
        } else {
            HFAUtils.showToast(activity, "Please check Internet Connection.");
        }
    }

    private void getLocalities(final MasterRequest masterRequest) {
        if (HFAUtils.isOnline(activity)) {
            SPSProgressDialog.showProgressDialog(activity);
            Log.v("LocalityInputs",masterRequest.getDcode()+",ftype"+masterRequest.getFtype()+",Man"+masterRequest.getMcode()+
                            ",RuralUrban"+masterRequest.getRuflag()+","+masterRequest.getVcode());
            ApiCall apiService = RestAdapter.createServiceWithAuth1(ApiCall.class);
            Call<LocalitiesResponse> call = apiService.getLocality(Constants.Username,Constants.Password,Constants.Session_Key,masterRequest);
            call.enqueue(new Callback<LocalitiesResponse>() {
                @Override
                public void onResponse(Call<LocalitiesResponse> call, Response<LocalitiesResponse> response) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.d("onResponse() - Response", response.body().toString());
                        if (!TextUtils.isEmpty(response.body().getStatus()) && response.body().getStatus().equalsIgnoreCase("Success")) {
                            //HFAUtils.showToast(activity, response.body().getStatus());
                            localitiesList.clear();
                            localitiesList = response.body().getMasterlist();
                           // Toast.makeText(MainActivity.this, "Lcaliti "+String.valueOf(localitiesList.size()), Toast.LENGTH_SHORT).show();
                            // Log.d("Localities_Length", String.valueOf(localitiesList.size()));
                        } else {
                            HFAUtils.showToast(activity, response.body().getReason());
                        }
                    } else {
                        APIError error = ErrorUtils.parseError(response);
                        Log.d("Error_Message", error.getMessage());
                        HFAUtils.showToast(activity, error.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<LocalitiesResponse> call, Throwable t) {
                    if (t instanceof SocketTimeoutException) {
                        getLocalities(masterRequest);
                    } else {
                        HFAUtils.showToast(activity, "Please Retry");
                        SPSProgressDialog.dismissProgressDialog();
                        Log.e("onFailure() - Response", t.getMessage());
                    }
                }
            });
        } else {
            HFAUtils.showToast(activity, "Please check Internet Connection.");
        }
    }

    public void submitData() {
        if (LoginType.equalsIgnoreCase("1")) {
            SubmitData submitData = new SubmitData();
            submitData.setDistrictCode(districtCode);
            submitData.setInstituteCode(selectedInstituteCode);
            submitData.setRuflag(selectedROrU);
            submitData.setMandalCode(selectedMandalCode);
            submitData.setVillageCode(villageCode);
            submitData.setLandMark(tvLandMark.getText().toString());
            submitData.setLocality_code(selectedLocalityCode);
            submitData.setSubmt_Mobile_No(Preferences.getIns().getMobileNo());
            submitData.setBuildingName(tvBuildingName.getText().toString());
            submitData.setStreetName(tvStreet.getText().toString());
            submitData.setGeO_LAT(prefUtils.getString("Latitude"));
            submitData.setGeO_LONG(prefUtils.getString("Longitude"));
            submitData.setNO_OF_CANDIDATES(tv_totalCandidatesAcc.getText().toString().trim());
            submitData.setGroundFloorRooms(edNoOfRoomsGroundFloor.getText().toString().trim());
            submitData.setGroundPersonsPerroom(edNoOfCandidatesGroundFloor.getText().toString().trim());
            submitData.setOtherFloorRooms(edNoOfRoomsOtherFloor.getText().toString().trim());
            submitData.setOtherPersonsPerroom(edNoOfCandidatesOtherFloor.getText().toString().trim());

            int checkedRadioButtonId = rg2.getCheckedRadioButtonId();
            if (checkedRadioButtonId == -1) {
                // No item selected
                submitData.setQuestioN_1("");

            } else {
                if (checkedRadioButtonId == R.id.rb2Yes) {
                    // Do something with the button
                    submitData.setQuestioN_1("YES");
                } else if (checkedRadioButtonId == R.id.rb2No) {
                    submitData.setQuestioN_1("No");
                }
            }

            int checkedRadioButtonId3 = rg3.getCheckedRadioButtonId();
            if (checkedRadioButtonId3 == -1) {
                // No item selected
                submitData.setQuestioN_2("");

            } else {
                if (checkedRadioButtonId3 == R.id.rb3Yes) {
                    // Do something with the button
                    submitData.setQuestioN_2("YES");
                } else if (checkedRadioButtonId3 == R.id.rb3No) {
                    submitData.setQuestioN_2("No");
                }

            }
            int checkedRadioButtonId4 = rg4.getCheckedRadioButtonId();
            if (checkedRadioButtonId4 == -1) {
                // No item selected
                submitData.setQuestioN_3("");

            } else {
                if (checkedRadioButtonId4 == R.id.rb4Yes) {
                    // Do something with the button
                    submitData.setQuestioN_3("YES");
                } else if (checkedRadioButtonId4 == R.id.rb4No) {
                    submitData.setQuestioN_3("No");
                }
            }


            int checkedRadioButtonId5 = rg5.getCheckedRadioButtonId();
            if (checkedRadioButtonId5 == -1) {
                // No item selected
                submitData.setQuestioN_4("");

            } else {
                if (checkedRadioButtonId5 == R.id.rb5Yes) {
                    // Do something with the button
                    submitData.setQuestioN_4("YES");
                } else if (checkedRadioButtonId5 == R.id.rb5No) {
                    submitData.setQuestioN_4("No");
                }
            }

            int checkedRadioButtonId6 = rg6.getCheckedRadioButtonId();
            if (checkedRadioButtonId6 == -1) {
                // No item selected
                submitData.setQuestioN_5("");

            } else {
                if (checkedRadioButtonId6 == R.id.rb6Yes) {
                    // Do something with the button
                    submitData.setQuestioN_5("YES");
                } else if (checkedRadioButtonId6 == R.id.rb6No) {
                    submitData.setQuestioN_5("No");
                }
            }

            int checkedRadioButtonId7 = rg7.getCheckedRadioButtonId();
            if (checkedRadioButtonId7 == -1) {
                // No item selected
                submitData.setQuestioN_6("");

            } else {
                if (checkedRadioButtonId7 == R.id.rb7Yes) {
                    // Do something with the button
                    submitData.setQuestioN_6("YES");
                } else if (checkedRadioButtonId7 == R.id.rb7No) {
                    submitData.setQuestioN_6("No");
                }
            }
            submitData.setImage_Path(imageBase64);
            if (selectedCenterCategory.equalsIgnoreCase("Any Other")) {
                submitData.setCenteR_CATEGORY(tvCategory.getText().toString());
            } else {
                submitData.setCenteR_CATEGORY(selectedCenterCategory);
            }
            Log.v("Submitting111",selectedInstituteCode+",Dis"+districtCode+","+",Mobile"+Preferences.getIns().getMobileNo());

            submittingData(submitData);
        } else {
            SubmitData submitData = new SubmitData();
            submitData.setDistrictCode(Preferences.getIns().getDistrictCode());
            submitData.setInstituteCode(selectedInstituteCode);
            submitData.setRuflag(selectedROrU);
            submitData.setMandalCode(selectedMandalCode);
            submitData.setVillageCode(villageCode);
            submitData.setLandMark(tvLandMark.getText().toString());
            submitData.setLocality_code(selectedLocalityCode);
            submitData.setSubmt_Mobile_No(Preferences.getIns().getMobileNo());
            submitData.setBuildingName(tvBuildingName.getText().toString());
            submitData.setStreetName(tvStreet.getText().toString());
            submitData.setGeO_LAT(prefUtils.getString("Latitude"));
            submitData.setGeO_LONG(prefUtils.getString("Longitude"));
            submitData.setNO_OF_CANDIDATES(tv_totalCandidatesAcc.getText().toString().trim());
            submitData.setGroundFloorRooms(edNoOfRoomsGroundFloor.getText().toString().trim());
            submitData.setGroundPersonsPerroom(edNoOfCandidatesGroundFloor.getText().toString().trim());
            submitData.setOtherFloorRooms(edNoOfRoomsOtherFloor.getText().toString().trim());
            submitData.setOtherPersonsPerroom(edNoOfCandidatesOtherFloor.getText().toString().trim());


            int checkedRadioButtonId = rg2.getCheckedRadioButtonId();
            if (checkedRadioButtonId == -1) {
                // No item selected
                submitData.setQuestioN_1("");

            } else {
                if (checkedRadioButtonId == R.id.rb2Yes) {
                    // Do something with the button
                    submitData.setQuestioN_1("YES");
                } else if (checkedRadioButtonId == R.id.rb2No) {
                    submitData.setQuestioN_1("No");
                }
            }

            int checkedRadioButtonId3 = rg3.getCheckedRadioButtonId();
            if (checkedRadioButtonId3 == -1) {
                // No item selected
                submitData.setQuestioN_2("");

            } else {
                if (checkedRadioButtonId3 == R.id.rb3Yes) {
                    // Do something with the button
                    submitData.setQuestioN_2("YES");
                } else if (checkedRadioButtonId3 == R.id.rb3No) {
                    submitData.setQuestioN_2("No");
                }

            }
            int checkedRadioButtonId4 = rg4.getCheckedRadioButtonId();
            if (checkedRadioButtonId4 == -1) {
                // No item selected
                submitData.setQuestioN_3("");

            } else {
                if (checkedRadioButtonId4 == R.id.rb4Yes) {
                    // Do something with the button
                    submitData.setQuestioN_3("YES");
                } else if (checkedRadioButtonId4 == R.id.rb4No) {
                    submitData.setQuestioN_3("No");
                }
            }


            int checkedRadioButtonId5 = rg5.getCheckedRadioButtonId();
            if (checkedRadioButtonId5 == -1) {
                // No item selected
                submitData.setQuestioN_4("");

            } else {
                if (checkedRadioButtonId5 == R.id.rb5Yes) {
                    // Do something with the button
                    submitData.setQuestioN_4("YES");
                } else if (checkedRadioButtonId5 == R.id.rb5No) {
                    submitData.setQuestioN_4("No");
                }
            }
            int checkedRadioButtonId6 = rg6.getCheckedRadioButtonId();
            if (checkedRadioButtonId6 == -1) {
                // No item selected
                submitData.setQuestioN_5("");

            } else {
                if (checkedRadioButtonId6 == R.id.rb6Yes) {
                    // Do something with the button
                    submitData.setQuestioN_5("YES");
                } else if (checkedRadioButtonId6 == R.id.rb6No) {
                    submitData.setQuestioN_5("No");
                }
            }

            int checkedRadioButtonId7 = rg7.getCheckedRadioButtonId();
            if (checkedRadioButtonId7 == -1) {
                // No item selected
                submitData.setQuestioN_6("");

            } else {
                if (checkedRadioButtonId7 == R.id.rb7Yes) {
                    // Do something with the button
                    submitData.setQuestioN_6("YES");
                } else if (checkedRadioButtonId7 == R.id.rb7No) {
                    submitData.setQuestioN_6("No");
                }
            }
            submitData.setImage_Path(imageBase64);
            if (selectedCenterCategory.equalsIgnoreCase("Any Other")) {
                submitData.setCenteR_CATEGORY(tvCategory.getText().toString());
            } else {
                submitData.setCenteR_CATEGORY(selectedCenterCategory);
            }
            Log.v("Submitting555",selectedInstituteCode+",Dis"+Preferences.getIns().getDistrictCode()+","+",Mobile"+Preferences.getIns().getMobileNo());

            submittingData(submitData);
        }
    }


    private void submittingData(final SubmitData masterRequest) {
        if (HFAUtils.isOnline(activity)) {
            SPSProgressDialog.showProgressDialog(activity);
            Log.v("Submitting",masterRequest.getInstituteCode()+",Dis"+masterRequest.getDistrictCode()+","+",Mobile"+Preferences.getIns().getMobileNo());
            ApiCall apiService = RestAdapter.createServiceWithAuth1(ApiCall.class);
            Call<SubmitResponse> call = apiService.submit(Constants.Username,Constants.Password,Constants.Session_Key,masterRequest);
            call.enqueue(new Callback<SubmitResponse>() {
                @Override
                public void onResponse(Call<SubmitResponse> call, Response<SubmitResponse> response) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.d("onResponse() - Response", response.body().toString());
                        if (!TextUtils.isEmpty(response.body().getStatus()) && response.body().getStatus().equalsIgnoreCase("Success")) {
                            HFAUtils.showToast(activity, response.body().getReason());
                            clearAllFields();
                        } else {
                            HFAUtils.showToast(activity, response.body().getReason());
                            //HFAUtils.showToast(activity, "Data Not Submitted");

                        }
                    } else {
                        APIError error = ErrorUtils.parseError(response);
                        Log.d("Error_Message", error.getMessage());
                        HFAUtils.showToast(activity, error.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<SubmitResponse> call, Throwable t) {
                    if (t instanceof SocketTimeoutException) {
                        submittingData(masterRequest);
                    } else {
                        HFAUtils.showToast(activity, "Please Retry");
                        SPSProgressDialog.dismissProgressDialog();
                        Log.e("onFailure() - Response", t.getMessage());
                    }
                }
            });
        } else {
            HFAUtils.showToast(activity, "Please check Internet Connection.");
        }
    }

    private void clearAllFields() {
        tvRorU.setText("");
        tvMandal.setText("");
        tvInstitute.setText("");
        tvCenterCategory.setText("");
        tvVillageOrWard.setText("");
        tvStreet.setText("");
        tvBuildingName.setText("");
        tvLandMark.setText("");
        tvLocality.setText("");
        tvCategory.setText("");
        imageBase64 = "";
        rg2.clearCheck();
        rg3.clearCheck();
        rg4.clearCheck();
        rg5.clearCheck();
        rg6.clearCheck();
        rg7.clearCheck();
        mandalList.clear();
        villagesList.clear();
        localitiesList.clear();
        edNoOfRoomsGroundFloor.setText("");
        edNoOfCandidatesGroundFloor.setText("");
        edNoOfRoomsOtherFloor.setText("");
        edNoOfCandidatesOtherFloor.setText("");
        tv_totalCandidatesAcc.setText("0");

        geoTaggedImage.setImageDrawable(getResources().getDrawable(R.mipmap.capture));
        View targetView = findViewById(R.id.tvDistrict);
        targetView.getParent().requestChildFocus(targetView, targetView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.logout:
                //logout code
                showLogoutDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void versionCheckService() {
        //HFAUtils.hideSoftKeyboard(activity);
        if (HFAUtils.isOnline(MainActivity.this)) {
            SPSProgressDialog.showProgressDialog(MainActivity.this);
            ApiCall apiService = RestAdapter.getClient().create(ApiCall.class);
            Call<VersionModel> call = apiService.checkversion(BuildConfig.VERSION_NAME);
            call.enqueue(new Callback<VersionModel>() {
                @Override
                public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                    try {
                        SPSProgressDialog.dismissProgressDialog();
                        if (!TextUtils.isEmpty(response.body().getStatus())
                                && response.body().getStatus().equalsIgnoreCase("Success")) {
                            // submitData();
                        } else {
                            HFAUtils.showToast(MainActivity.this, response.body().getReason());
                            openUpdateDialog();

                        }
                    } catch (Exception e) {
                        // HFAUtils.showToast(MainActivity.this, "Network /Server related Issue. Please Try Again Later.");
                    }
                }

                @Override
                public void onFailure(Call<VersionModel> call, Throwable t) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (t instanceof SocketTimeoutException) {
                        versionCheckService();
                    } else {
                        HFAUtils.showToast(MainActivity.this, "Unable to check app_version.");
                        Log.e("failure", "failure");
                    }
                }
            });
        } else {
            HFAUtils.showToast(MainActivity.this, "Please check Internet Connection.");
        }
    }

    private void versionCheckService1() {
        //HFAUtils.hideSoftKeyboard(activity);
        if (HFAUtils.isOnline(MainActivity.this)) {
            SPSProgressDialog.showProgressDialog(MainActivity.this);
            ApiCall apiService = RestAdapter.getClient().create(ApiCall.class);
            Call<VersionModel> call = apiService.checkversion(BuildConfig.VERSION_NAME);
            call.enqueue(new Callback<VersionModel>() {
                @Override
                public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                    try {
                        SPSProgressDialog.dismissProgressDialog();
                        if (!TextUtils.isEmpty(response.body().getStatus())
                                && response.body().getStatus().equalsIgnoreCase("Success")) {
                            submitData();
                        } else {
                            HFAUtils.showToast(MainActivity.this, response.body().getReason());
                            openUpdateDialog();

                        }
                    } catch (Exception e) {
                        // HFAUtils.showToast(MainActivity.this, "Network /Server related Issue. Please Try Again Later.");
                    }
                }

                @Override
                public void onFailure(Call<VersionModel> call, Throwable t) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (t instanceof SocketTimeoutException) {
                        versionCheckService1();
                    } else {
                        HFAUtils.showToast(MainActivity.this, "Unable to check app_version.");
                        Log.e("failure", "failure");
                    }
                }
            });
        } else {
            HFAUtils.showToast(MainActivity.this, "Please check Internet Connection.");
        }
    }

    private void openUpdateDialog() {
        new android.app.AlertDialog.Builder(this)
                .setMessage("You're using older version please update the app")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Preferences.getIns().clear();
                        ActivityCompat.finishAffinity(MainActivity.this);
                    }
                })
                .show();
    }


    private void showLogoutDialog() {
        new android.app.AlertDialog.Builder(this)
                .setMessage("Are you sure you want to Logout?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Preferences.getIns().clear();
                        Intent newIntent = new Intent(MainActivity.this, LoginActivity.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent);
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }
}



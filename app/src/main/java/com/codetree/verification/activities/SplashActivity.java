package com.codetree.verification.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.codetree.verification.BuildConfig;
import com.codetree.verification.R;
import com.codetree.verification.models.VersionModel;
import com.codetree.verification.utils.HFAUtils;
import com.codetree.verification.utils.Preferences;
import com.codetree.verification.utils.SPSProgressDialog;
import com.codetree.verification.webservices.ApiCall;
import com.codetree.verification.webservices.RestAdapter;


import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    private static final int REQUEST_MULTIPLE_PERMISSIONS = 123;
    private static final String TAG = SplashActivity.class.getSimpleName();
    TextView splashTv;
    SplashActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeFullScreenActivity();
        activity = this;
        setContentView(R.layout.activity_splash);
        init();
        checkAndroidVersion();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    //To initialise views
    void init() {
        splashTv = findViewById(R.id.tv_splash_text);
    }

    //To check android version
    void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d(TAG,"checkAndroidVersion() : Running Device Version is More than or equal to Android v5.0");
            if (isRequiredPermissionsAdded()) {
                Log.d(TAG,"Required Permissions Already Added.");
                versionCheckService();
            }
            else 
            {
                Log.d(TAG,"Required Permissions need to be added.");
                isRequiredPermissionsAdded();
            }
        } else {
            Log.d(TAG,"checkAndroidVersion() : Running Device Version is Less than Android v5.0");
            versionCheckService();
        }
    }

    //Navigate to next screen after 3sec
    private void navigateToNextScreen() {
        setAnimation(this, R.anim.alpha, splashTv);

                if (Preferences.getIns().getRemeberMe()) {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
    }

    //To Make Full screen Activity
    public void makeFullScreenActivity() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    //TO check and request multiple runtime permissions
    private boolean isRequiredPermissionsAdded() {
        String[] permissions = new String[]
                {
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_PHONE_STATE,
                                        };
        List<String> listOfPermissionsNeeded = new ArrayList<>();
        int result;
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNeeded.add(permission);
            }
        }
        if (!listOfPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listOfPermissionsNeeded.toArray(new String[listOfPermissionsNeeded.size()]), REQUEST_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    //Set Animation
    public void setAnimation(Activity mActivity, int animationResourceId, View view) {

        Animation a = AnimationUtils.loadAnimation(mActivity, animationResourceId);
        view.setAnimation(a);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_MULTIPLE_PERMISSIONS) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "onRequestPermissionsResult : " + permissions[i] + " is granted.");
                    } else
                        Log.d(TAG, "onRequestPermissionsResult : " + permissions[i] + " is denied.");
                }
                versionCheckService();
            }

        }
    }
    private void versionCheckService() {
        //HFAUtils.hideSoftKeyboard(activity);
        if (HFAUtils.isOnline(activity)) {
            SPSProgressDialog.showProgressDialog(activity);
            ApiCall apiService = RestAdapter.getClient().create(ApiCall.class);
            Call<VersionModel> call = apiService.checkversion(BuildConfig.VERSION_NAME);
            call.enqueue(new Callback<VersionModel>() {
                @Override
                public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                    try {
                       SPSProgressDialog.dismissProgressDialog();
                        if (!TextUtils.isEmpty(response.body().getStatus())
                                && response.body().getStatus().equalsIgnoreCase("Success")) {
                            //callLoginService();
                            // moveNext();
                            Log.v("Responseee",response.body().getStatus());
                            navigateToNextScreen();

                        } else {
                            HFAUtils.showToast(activity, response.body().getReason());
                            openUpdateDialog();
                        }
                    } catch (Exception e) {
                        HFAUtils.showToast(activity, "Network /Server related Issue. Please Try Again Later.");
                    }
                }

                @Override
                public void onFailure(Call<VersionModel> call, Throwable t) {
                    SPSProgressDialog.dismissProgressDialog();
                    if (t instanceof SocketTimeoutException) {
                        versionCheckService();
                    } else {
                        HFAUtils.showToast(activity, "Unable to check app_version.");
                        Log.e("failure", "failure");
                    }
                }
            });
        } else {
            HFAUtils.showToast(activity, "Please check Internet Connection.");
        }
    }

    private void openUpdateDialog() {
        new android.app.AlertDialog.Builder(this)
                .setMessage("You're using older version please update the app")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Preferences.getIns().clear();
                        ActivityCompat.finishAffinity(SplashActivity.this);

                    }
                })
                .show();

    }

}

package com.codetree.verification.samplecodes;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.codetree.verification.R;

import java.io.ByteArrayOutputStream;

public class CaptureOrChooseImage extends AppCompatActivity {
    private static final String TAG = CaptureOrChooseImage.class.getSimpleName();
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE_GALLERY = 201;
    String profileBase64 = "";
    ImageView imgProfilePicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_or_choose_image);
        imgProfilePicture =findViewById(R.id.imageView);
        imgProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();
            }
        });
    }
    void showPopup() {
        final CharSequence[] items = {"Photo from Camera", "Photo from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Media");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the dialog_with_list
                String i = String.valueOf(items[item]);
                if ("Photo from Camera".equalsIgnoreCase(i)) {
                    Log.d(TAG, "Photo---" + i);
                    openCamera(CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                } else if ("Photo from Gallery".equalsIgnoreCase(i)) {
                    Log.d(TAG, "Video---" + i);
                    chooseFromGallery(CAMERA_CAPTURE_IMAGE_REQUEST_CODE_GALLERY);
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void chooseFromGallery(int i) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, i);
    }

    private void openCamera(int request) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, request);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            try {
                Bitmap mphoto = (Bitmap) data.getExtras().get("data");
                profileBase64 = getBase64String(mphoto);
                Log.d(TAG, "Start Image base 64--" + profileBase64);
                if (mphoto != null)
                    imgProfilePicture.setImageBitmap(mphoto);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (resultCode == RESULT_OK && requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE_GALLERY) {
            try {
                if (resultCode == RESULT_OK) {
                    try {
                        Uri selectedImage = data.getData();
                        profileBase64 = getGalleryImageBase64(selectedImage, imgProfilePicture);
                        Log.d(TAG, "Image base 64--" + profileBase64);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    String getGalleryImageBase64(Uri uri, ImageView imageView) {
        String base64Str = "";
        try {
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c = getContentResolver().query(uri, filePath, null, null, null);
            if (c != null) {
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                /*Decode a file path into a bitmap. If the specified file name is null,
                 or cannot be decoded into a bitmap, the function returns null.*/
                Bitmap mphoto = (BitmapFactory.decodeFile(picturePath));
                if (mphoto != null) {
                    imageView.setImageBitmap(mphoto);
                    Log.w(TAG, "Image gallery--" + picturePath);
                    // Bitmap newBitp = getResizedBitmap(mphoto, 480, 640);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    mphoto.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64Str = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64Str;
    }
    public static String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }
}

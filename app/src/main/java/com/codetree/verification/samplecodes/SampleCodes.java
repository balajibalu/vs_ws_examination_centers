package com.codetree.verification.samplecodes;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.codetree.verification.R;
import com.codetree.verification.activities.MainActivity;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*  1.Read text file from assets folder into textView
    2.Checking Internet Connection
    3.Navigate to other Activity
    4.Showing progress dialog
    5.Displaying Toast message
    6.Send Email
    7.show keyboard
    8.Hide Keyboard
    9.Showing Alert Dialog
    10.Add fragment to Activity
    11.Get Current Fragment
    12.Remove Fragment from Activity
    13.Replace Fragment
    14.Send Data from Activity to Fragment
    15.Convert Image to Bitmap
    16.Email Validation
    17.To make capitalize the string
    18.File Reading into String array
    19.Gps Checking
    20.Show Alert to Enable Location
    21.Phone Call
    22.Location using lat lang
    23.Convert ImageView to String
    24.Convert Bitmap to String
    25.Send SMS
    26.Blink Animation
    27.Set Animation
    28.Handler to do a task after some time
    29.Thread to perform background task and interact with UI
    30.DatePicker Dialog
    31.Time Picker Dialog
    32.To Make particular url by adding base url and api end point
    33.Email Validation short method
    34.Email Validation Long Method
    35.Mobile Number Validation short method
    36.Mobile Number Validation Long method
    37.Add Multiple Runtime permissions
    38.To Make Full screen Activity
    39.To Display Spinner Data
    40.To Display Simple List
    41.Dynamic Check Boxes
    42.Show Dialog with List
    43.Current Date
    44.Share Data through intent
    45.LogOut method
    46.Create Table
    47.Dismiss Progress Dialog
*/
public class SampleCodes extends AppCompatActivity {


    private static final String TAG = SampleCodes.class.getSimpleName();
    private static final int REQUEST_MULTIPLE_PERMISSIONS = 123;
    CheckBox[] checkBoxList;
    Dialog dg;
    private ListView lv_data;
    SampleCodes mActivity;
    LinearLayout ll_user_details;
    TextView[] tv_member_name, tv_id, tv_mobile, mem_gpward, mem_status;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;

    }

    //Read text file from assets folder into textView
    void readDoc(TextView textView) {
        String text = "";
        try {
            InputStream inputStream = getAssets().open("FAQ.txt");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            text = new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        textView.setText(text);
    }

    //Checking Internet Connection
    public static boolean isOnline(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isNetworkConnected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        return isNetworkConnected;
    }

    //Navigate to other Activity
    public void navigateActivity(Activity source, Class<?> destination, Bundle data) {
        Intent intent = new Intent(source, destination);
        if (data != null && !data.isEmpty()) {
            intent.putExtra("KEY", data);
        }
        Log.d(TAG, "Navigating from " + source.getClass().getSimpleName() + " to " + destination.getSimpleName());
        startActivity(intent);
    }

    // Showing progress dialog
    // ProgressDialog mDialog = new ProgressDialog(context);
    public ProgressDialog showProgressDialog(Context context, String message, ProgressDialog mDialog) {
        mDialog.setMessage(message);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(false);
        mDialog.show();
        return mDialog;
    }

    // Displaying Toast message
    public static void showToast(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    //Send Email
    public void sendEmail(Activity activity, String response) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"venkateshb.codetree@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Response");
        intent.putExtra(Intent.EXTRA_TEXT, response);
        try {
            activity.startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "There are btnNo email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    //show keyboard
    public void showKeyboard(Activity activity) {
        if (activity != null) {
            activity.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    //Hide Keyboard
    public void hideKeyboard(Activity activity) {
        if (activity != null) {
            activity.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    //Showing Alert Dialog
    public void showAlertDialog(final Activity activity, String title, String message, String pClick, String nClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);

        builder.setTitle(title).setMessage(message)
                .setPositiveButton(pClick, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do something

                    }
                }).setNegativeButton(nClick, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //Add fragment to Activity
    public void addFragment(int contentAreaId, Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(contentAreaId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        } else {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        Log.d(TAG, fragment.getClass().getSimpleName() + " Added Successfully");
    }

    // Get Current Fragment
    public Fragment getCurrentFrag(int contentAreaId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        return fragmentManager.findFragmentById(contentAreaId);
    }

    //Remove Fragment from Activity
    public void removeFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment = (Fragment) fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName());
        if (fragment != null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.remove(fragment);
            transaction.commit();
            Log.d(TAG, fragment.getClass().getSimpleName() + " " + "has been removed");
        }
    }

    //Replace Fragment
    public void replaceFragment(int id, Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(id, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        } else {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        Log.d(TAG, "Previous fragment replaced with " + fragment.getClass().getSimpleName());
    }

    //Send Data from Activity to Fragment
    public void sendDataToFragment(Fragment fragment, Bundle data) {
        if (data != null && !data.isEmpty()) {
            fragment.setArguments(data);
        }
    }

    //Convert Image to Bitmap
    public Bitmap convertBase64ToBitmap(String imageUrl) {
        byte[] decodedString = Base64.decode(imageUrl, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    //Email Validation
    public boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    //To make capitalize the string
    public String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }
        return capMatcher.appendTail(capBuffer).toString();
    }

    // File Reading into String array
    public String[] FileReading(Activity av, int filename) {
        StringBuffer buf = new StringBuffer();
        InputStream is = null;
        String arr[] = null;
        try {
            String str = "";
            is = av.getResources().openRawResource(filename);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is));
            if (is != null) {
                while ((str = reader.readLine()) != null) {
                    buf.append(str + "\n");
                }
            }
            arr = buf.toString().split("\\n");
        } catch (IOException e) {
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException io) {
            }
        }
        return arr;
    }


    //Gps Checking
    void gpsChecking() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // check if GPS enabled
        if (locationManager != null) {
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!isGPSEnabled)
                showAlert();
            else
                Log.d(TAG, "GPS Enabled");
        }
    }

    //Show Alert to Enable Location
    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("GPS is not enabled. Please enable GPS")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    //Phone Call
    public void makePhoneCall(final Activity mActivity, final String telephoneNumber) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Proceed to Call");
        builder.setMessage("Do you need any help ?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + telephoneNumber));
                if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CALL_PHONE}, 1);
                    return;
                }
                mActivity.startActivity(callIntent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //Location using lat lang
    public void showLocation(Activity mActivity, String latitude, String longitude) {
        if (!latitude.isEmpty() && !longitude.isEmpty()) {
            String uri = "http://maps.google.com/maps?q=" + Double.parseDouble(latitude.substring(0, latitude.length() - 1)) + "," + Double.parseDouble(longitude.substring(0, longitude.length() - 1)) + "(" + " " + ")&iwloc=A&hl=es";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(intent);
        } else {
            Toast.makeText(mActivity, "No latitude and longitude found", Toast.LENGTH_SHORT).show();
        }

    }

    //Convert ImageView to String
    public static String ImageViewToString(ImageView imageView) {
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        String base64String = getBase64String(drawable.getBitmap());
        return base64String;
    }

    //Convert Bitmap to String
    public static String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    //Send SMS
    public void sendSMS(String phoneNo, String msg, Context context) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(context, "Message Sent", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    //Blink Animation
    public static void blinkTextView(View view) {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(150); //You can manage the time of the blink with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        view.startAnimation(anim);
    }

    //Set Animation
    public void setAnimation(Activity mActivity, int animationResourceId, View view) {

        Animation a = AnimationUtils.loadAnimation(mActivity, animationResourceId);
        view.setAnimation(a);
    }

    //Handler to do a task after some time
    public void createHandler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do Something

                finish();

            }
        }, 3000);
    }

    //Thread to perform background task and interact with UI
    public void createThread() {
        Thread backGroundThread = new Thread(new Runnable() {
            @Override
            public void run() {
                //To interact with UI we Need runOnUIThread() because UI can be updated by Only Main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //we can interact with ui here
                    }
                });

            }
        });
    }

    //DatePicker Dialog
    public void showDatePickerDialog() {
        int mYear, mMonth, mDay, mHour, mMinute;
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = null;
        datePickerDialog = new DatePickerDialog(SampleCodes.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Log.d(TAG, "Selected Date : " + String.valueOf(dayOfMonth) + " - " + String.valueOf(monthOfYear + 1) + " - " + String.valueOf(year));
                    }
                }, mYear, mMonth, mDay);
       /*Disable Future Dates :
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());*/
       /* Disable Past Dates :
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());*/
        datePickerDialog.show();
    }


    //Time Picker Dialog
    public void showTimePickerDialog() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(SampleCodes.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Log.d(TAG, "Selected Time : " + selectedHour + " : " + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    //To Make particular url by adding base url and api end point
    public String getUrl(String baseUrl, String subUrl) {
        StringBuilder serviceUrl = new StringBuilder(baseUrl);
        serviceUrl.append(subUrl);
        Log.d(TAG, "Generated url : " + serviceUrl.toString());
        return serviceUrl.toString();
    }

    //Email Validation short method
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    //Email Validation Long Method
    public void emailValidation(EditText emailValidate) {
        final String email = emailValidate.getText().toString().trim();

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        emailValidate.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (email.matches(emailPattern) && s.length() > 0) {
                    Toast.makeText(getApplicationContext(), "valid email address", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // other stuffs
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // other stuffs
            }
        });
    }

    //Mobile Number Validation short method
    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    //Mobile Number Validation Long method
    private boolean isMobileValid(String mobile) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", mobile)) {
            if (mobile.length() < 10 || mobile.length() > 12) {
                // if(mobile.length() != 10) {
                Toast.makeText(SampleCodes.this, "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
            } else {
                check = true;
            }
        }
        return check;
    }


    //Add Multiple Runtime permissions
    private boolean checkAndRequestPermissions() {
        String[] permissions = new String[]
                {
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_CALENDAR,
                        Manifest.permission.READ_PHONE_STATE
                };
        List<String> listOfPermissionsNeeded = new ArrayList<>();
        int result;
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNeeded.add(permission);
            }
        }
        if (!listOfPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listOfPermissionsNeeded.toArray(new String[listOfPermissionsNeeded.size()]), REQUEST_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_MULTIPLE_PERMISSIONS) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "onRequestPermissionsResult : " + permissions[i] + " is granted.");
                        navigateToNextScreen();
                    } else
                        Log.d(TAG, "onRequestPermissionsResult : " + permissions[i] + " is denied.");
                }

            }
        }
    }

    private void navigateToNextScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               /* Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);*/
            }
        }, 3000);
    }

    //To Make Full screen Activity
    public void makeFullScreenActivity() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    //To Display Spinner Data
    public Spinner displaySpinnerData(Activity activity, Spinner spinner, String[] data) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(arrayAdapter);
        return spinner;
    }

    //To Display Simple List
    public ListView displaySimpleList(Activity activity, ListView listView, String[] data) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, data);
        listView.setAdapter(arrayAdapter);
        return listView;
    }

    //Dynamic Check Boxes
    /* 1.Create Empty LinearLayout to display custom checkbox
    2.create custom checkbox in separate layout file.
    3.Need to define Checkbox[] checkBoxList; array Locally*/
    public void createDynamicCheckBoxes(LinearLayout cbLayout, String[] cbTitles) {
        //remove all views if any in the parent view where we load custom views
        cbLayout.removeAllViews();
        int listSize = cbTitles.length;
        //let we know how many custom views needed by using list size
        checkBoxList = new CheckBox[listSize];
        List<String> selectedCBList = new ArrayList<>();

        for (int i = 0; i < listSize; i++) {
            //create custom view
            View view = this.getLayoutInflater().inflate(R.layout.custom_checkbox, null);
            checkBoxList[i] = view.findViewById(R.id.customCB);
            checkBoxList[i].setText(cbTitles[i]);
            final int i1 = i;
            checkBoxList[i1].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        // selectedCBList.add(checkBoxList[i1].getText().toString());
                        checkBoxList[i1].setChecked(true);
                    } else {
                        checkBoxList[i1].setChecked(false);
                        /*selectedCBList.remove(checkBoxList[i1].getText().toString());
                        builder.setLength(0);*/
                    }
                }
            });
            cbLayout.addView(view);
        }

    }

    //Show Dialog with List
    private void showDialogWithList(final int index) {
        List<String> list = new ArrayList<>();
        System.out.println("Entered Index is...." + index);
        try {
            System.out.print("Entered try..........");
            dg = new Dialog(mActivity);
            dg.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dg.setContentView(R.layout.dialog_with_list);
            TextView tv_header = (TextView) dg
                    .findViewById(R.id.tv_selecion_header);
            dg.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            lv_data = (ListView) dg.findViewById(R.id.list_selection);
            if (index == 1) {
                tv_header.setText("Select Date *");
                ArrayAdapter<String> adp = new ArrayAdapter<String>(
                        mActivity, R.layout.dialog_with_list_item,
                        R.id.tv_list_adapetr, list);
                lv_data.setAdapter(adp);
                lv_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (parent == lv_data) {
                            String selectedPosition = parent.getItemAtPosition(position).toString();
                            System.out.print("\n" + "Selection of Position......................" + selectedPosition);
                            dg.cancel();

                        }
                    }
                });
            }
            if (index == 2) {
                tv_header.setText("Select Slot*");
                List<String> list1 = new ArrayList<>();
                ArrayAdapter<String> adp = new ArrayAdapter<String>(
                        mActivity, R.layout.dialog_with_list_item,
                        R.id.tv_list_adapetr, list1);
                lv_data.setAdapter(adp);
                lv_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (parent == lv_data) {
                            String selectedPosition = parent.getItemAtPosition(position).toString();
                            System.out.print("\n" + "Selection of position......................" + selectedPosition);
                            dg.cancel();
                        }
                    }
                });
            }
            dg.setCancelable(true);
            dg.show();
        } catch (Exception e) {
            System.out.print("Exception.........." + e.getMessage());
        }

    }

    //Current Date
    private String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
        String currentDate = df.format(c);
        Log.d("Current_Date", currentDate);
        return currentDate;
    }

    //Share Data through intent
    public void shareData(String shareText, String shareBody) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareText);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, ""));
    }

    //LogOut method
    private void showLogoutDialog(String Message) {
        new AlertDialog.Builder(this)
                .setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Make sure delete shared preferences
                        /*Preferences.getIns().clear();
                        deletePreferences(this);*/
                        Intent newIntent = new Intent(SampleCodes.this, MainActivity.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent);
                        SampleCodes.this.finish();
                        //   finishAffinity();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    //Create Table
    private void createTable(final List<?> memberData) {

        ll_user_details.removeAllViews();
        tv_id = new TextView[memberData.size()];
        tv_member_name = new TextView[memberData.size()];
        tv_mobile = new TextView[memberData.size()];
        mem_status = new TextView[memberData.size()];
        mem_gpward = new TextView[memberData.size()];

        for (int i = 0; i < memberData.size(); i++) {
            View v = this.getLayoutInflater().inflate(R.layout.table_row, null);

            TextView mem_gender, mem_sno;
            final int finalI = i;
            mem_sno = (TextView) v.findViewById(R.id.mem_sno);
            tv_id[i] = (TextView) v.findViewById(R.id.mem_id);
            tv_member_name[i] = (TextView) v.findViewById(R.id.mem_name);
            tv_mobile[i] = (TextView) v.findViewById(R.id.mem_mobile);
            mem_status[i] = (TextView) v.findViewById(R.id.mem_status);
            mem_gender = (TextView) v.findViewById(R.id.mem_gender);
            mem_gpward[i] = (TextView) v.findViewById(R.id.mem_gp_ward);

            mem_sno.setText("" + (i + 1));

/*
            if (memberData.get(i).getVV_ID() != null && memberData.get(i).getVV_ID() != "") {
                tv_id[i].setText(memberData.get(i).getVV_ID());
                tv_id[i].setPaintFlags(tv_id[i].getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                tv_id[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, AttendanceActivity.class);
                        intent.putExtra("Member_data", memberData.get(finalI));
                        startActivityForResult(intent, 1);

                    }
                });
*/

            ll_user_details.addView(v);
        }
    }

    //Dismiss Progress Dialog
    public static void dismissProgressDialog(ProgressDialog mProgressDialog) {
        try {
            if (null != mProgressDialog) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

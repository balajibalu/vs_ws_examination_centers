package com.codetree.verification.samplecodes;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by mahiti on 13/2/17.
 */
public class ReminderClass {
    private static final String TAG="RemainderClass";


    // Add an event to the calendar of the user.
    public static void addEvent(Activity context, int _year, int _month, int _day, int _hour, int _minute, String title, String desc) {
        Log.d(TAG, "Event Added On : " + _year +"-"+  _month + "-" + _day + " " + _hour + ":" + _minute + " " );
        int currentMonth=_month-1;
        GregorianCalendar calDate = new GregorianCalendar(_year, currentMonth , _day, _hour, _minute);

        try {
            Log.d(TAG, "addEvent getTimeInMillis : " + calDate.getTimeInMillis());
            Log.d(TAG, "addEvent Title : " + title);
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.DTSTART, calDate.getTimeInMillis());
            values.put(CalendarContract.Events.DTEND, calDate.getTimeInMillis() + 60 * 60 * 1000);
            values.put(CalendarContract.Events.TITLE, title);
            values.put(CalendarContract.Events.CALENDAR_ID, 1);
            values.put(CalendarContract.Events.DESCRIPTION, desc);
            //values.put(CalendarContract.Events.EVENT_LOCATION, location);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
            System.out.println(Calendar.getInstance().getTimeZone().getID());
            Log.d(TAG,Calendar.getInstance().getTimeZone().getID());

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
           Log.d(TAG, "addEvent values : " + values);
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
            // yyyy-MM-dd HH:mm:ss

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            String reminderDate = df.format(calDate.getTime());
            Log.d(TAG,"Remainder Date :"+reminderDate);


            // get the event ID that is the last element in the Uri
           long _eventId = Long.parseLong(uri.getLastPathSegment());

           Log.d(TAG, "addEvent _eventId-->" +_eventId);


            Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
            builder.appendPath("time");
            ContentUris.appendId(builder, calDate.getTimeInMillis());
            setReminder(context, cr, _eventId, 60);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // routine to add reminders with the event
    public static void setReminder(Activity context, ContentResolver cr, long eventID, int timeBefore) {
        try {
           Log.d(TAG, "setReminder eventID-->" + eventID);
            // Uri REMINDERS_URI = Uri.parse(getCalendarUriBase(context) + "reminders");
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Reminders.MINUTES, timeBefore);
            values.put(CalendarContract.Reminders.EVENT_ID, eventID);
            values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            // values.put(CalendarContract.Reminders.DESCRIPTION, "Hello mai Description");
            //values.put(CalendarContract.Reminders.MINUTES, 1);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
           Log.d(TAG, "setReminder values-->" + values);

            //   Uri REMINDERS_URI = Uri.parse(getCalendarUriBase(true) + "reminders");

            //  Uri uri = cr.insert(REMINDERS_URI, values);

            Uri uri = cr.insert(CalendarContract.Reminders.CONTENT_URI, values);
            Cursor c = CalendarContract.Reminders.query(cr, eventID,
                    new String[]{CalendarContract.Reminders.MINUTES});
            if (c.moveToFirst()) {
                System.out.println("calendar"
                        + c.getInt(c.getColumnIndex(CalendarContract.Reminders.MINUTES)));
            }
            c.close();
           Log.d(TAG, "setReminder Cursor");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Returns Calendar Base URI, supports both new and old OS.
     */
    private static String getCalendarUriBase(boolean eventUri) {
        Uri calendarURI = null;
        try {
            if (Build.VERSION.SDK_INT <= 7) {
                calendarURI = (eventUri) ? Uri.parse("content://calendar/") : Uri.parse("content://calendar/calendars");
            } else {
                calendarURI = (eventUri) ? Uri.parse("content://com.android.calendar/") : Uri
                        .parse("content://com.android.calendar/calendars");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendarURI.toString();
    }

}

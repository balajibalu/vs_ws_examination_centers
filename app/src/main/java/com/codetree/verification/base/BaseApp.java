package com.codetree.verification.base;

import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

/**
 * Created by codetree on 3/24/2017.
 */

public class BaseApp extends MultiDexApplication {
    private static BaseApp instance;

    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }

    public BaseApp() {
        instance = this;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    /**
     * getting instance of Applivcation Context
     *
     * @return
     */
    public static BaseApp get() {
        return instance;
    }


    public  void setContext(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}

package com.codetree.verification.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;


import com.codetree.verification.utils.Constants;
import com.codetree.verification.utils.CustomProgressDialog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
 * 1.Check whether the Internet is there or not
 * 2.Navigate to other Activity
 * 3.Retrieve Bundle from Activity
 * 4.Showing progress dialog
 * 5.To check permission
 * 6.To make request for required permission
 * 7.To Show Long Toast
 * 8.To Show short Toast
 * 9.To send Email
 * 10.show keyboard
 * 11.Hide Keyboard
 * 12.showing Alert Dialog
 * 13.performing alertDialog positive button action through interface
 * 14.Add fragment to Activity
 * 15.Get Current Fragment
 * 16.Remove Fragment from Activity
 * 17.Replace Fragment
 * 18.Send Data from Activity to Fragment
 * 19.Convert Image to Bitmap
 * 20.Email Validation
 * 21.To make capitalize the string
 * 22.File Reading
 * 23.To Make particular url by adding base url and api end point
 * 24.Gps Checking
 * 25.Shared Preferences
 * 26.To Display Spinner Data
 * 27.To Display Simple List*/

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "Base Activity";
    public static final String PREF_NAME = "RERA_PREFERENCES";
    private static final int MODE = Context.MODE_PRIVATE;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = this.getSupportFragmentManager();
    }

    //Check whether the Internet is there or not
    public boolean isOnline(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null) && activeNetworkInfo.isConnected();
    }

    //Navigate to other Activity
    public void navigateActivity(Activity source, Class<?> destination, Bundle data) {
        Intent intent = new Intent(source, destination);
        if (data != null && !data.isEmpty()) {
            intent.putExtra(Constants.EXTRA_DATA, data);
        }
        Log.d(TAG, "Navigating from " + source.getClass().getSimpleName() + " to " + destination.getSimpleName());
        startActivity(intent);
    }

    //Retrieve Bundle from Activity
    public Bundle getBundleFromActivity(Activity mActivity) {
        Log.d(TAG, "Bundle is Requested From : " + mActivity.getClass().getSimpleName());
        return mActivity.getIntent().getBundleExtra(Constants.EXTRA_DATA);
    }

    // Showing progress dialog
    //ProgressDialog mDialog = new ProgressDialog(context);
    public void showProgressDialog(Context context, CustomProgressDialog mDialog) {
        mDialog.setCancelable(false);
        mDialog.show();
    }

    //Dismiss Progress Dialog
    public static void dismissProgressDialog(CustomProgressDialog mProgressDialog) {
        try {
            if (null != mProgressDialog) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //To check permission
    public boolean checkPermission(Activity activity, String permission) {
        boolean flag;
        flag = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
        return flag;
    }

    //To make request for required permission
    public void requestPermission(Activity activity, String permission) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, 1);
    }

    // To Show Long Toast
    public static void showLongToast(Activity activity, String str) {
        Toast.makeText(activity, str, Toast.LENGTH_LONG).show();
    }

    //To show short toast
    public static void showShortToast(Context activity, String str) {
        Toast.makeText(activity, str, Toast.LENGTH_SHORT).show();
    }


    //To send Email
    public void sendEmail(Activity activity, String response) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"venkateshb.codetree@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Response");
        intent.putExtra(Intent.EXTRA_TEXT, response);
        try {
            activity.startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    //show keyboard
    public void showKeyboard(Activity activity) {
        if (activity != null) {
            activity.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    //Hide Keyboard
    public void hideKeyboard(Activity activity) {
        if (activity != null) {
            activity.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    //showing Alert Dialog
    public void showAlertDialog(final Activity activity, String title, String message, String pClick, String nClick, final AlertDialogAction alertDialogAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);

        builder.setTitle(title).setMessage(message).setPositiveButton(pClick, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialogAction.alertDialogPositiveBtnClicked(true);
            }
        }).setNegativeButton(nClick, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //performing alertDialog positive button action through interface
    public interface AlertDialogAction {
        void alertDialogPositiveBtnClicked(boolean clicked);
    }

    //Add fragment to Activity
    public void addFragment(int contentAreaId, Fragment fragment, boolean addToBackStack) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(contentAreaId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        } else {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        Log.d(TAG, fragment.getClass().getSimpleName() + " Added Successfully");
    }

    // Get Current Fragment
    public Fragment getCurrentFrag(int contentAreaId) {
        return fragmentManager.findFragmentById(contentAreaId);
    }

    //Remove Fragment from Activity
    public void removeFragment(Fragment fragment) {
        fragment = (Fragment) fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName());
        if (fragment != null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.remove(fragment);
            transaction.commit();
            Log.d(TAG, fragment.getClass().getSimpleName() + " " + "has been removed");
        }
    }

    //Replace Fragment
    public void replaceFragment(int id, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(id, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        } else {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        Log.d(TAG, "Previous fragment replaced with " + fragment.getClass().getSimpleName());
    }

    //Send Data from Activity to Fragment
    public void sendDataToFragment(Fragment fragment, Bundle data) {
        if (data != null && !data.isEmpty()) {
            fragment.setArguments(data);
        }
    }

    // Convert Image to Bitmap
    public Bitmap convertBase64ToBitmap(String imageUrl) {
        byte[] decodedString = Base64.decode(imageUrl, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    //Email Validation
    public boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    //To make capitalize the string
    public String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }
        return capMatcher.appendTail(capBuffer).toString();
    }

    // File Reading
    public String[] FileReading(Activity av, int filename) {
        StringBuffer buf = new StringBuffer();
        InputStream is = null;
        String arr[] = null;
        try {
            String str = "";
            is = av.getResources().openRawResource(filename);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is));
            if (is != null) {
                while ((str = reader.readLine()) != null) {
                    buf.append(str + "\n");
                }
            }
            arr = buf.toString().split("\\n");
        } catch (IOException e) {
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException io) {
            }
        }
        return arr;
    }

    //To Make particular url by adding base url and api end point
    public String getUrl(String baseUrl, String subUrl) {
        StringBuilder serviceUrl = new StringBuilder(baseUrl);
        serviceUrl.append(subUrl);
        Log.d(TAG, "Generated url : " + serviceUrl.toString());
        return serviceUrl.toString();
    }

    //Gps Checking
    void gpsChecking() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // check if GPS enabled
        if (locationManager != null) {
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!isGPSEnabled)
                showAlert();
            else
                Log.d(TAG, "GPS Enabled");
        }
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("GPS is not enabled. Please enable GPS")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    /*.................................Shared Preferences - Start.......................................*/

    public void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();

    }

    public int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();
    }

    public String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    public long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    // Set Remember to user
    public void setRememberMe(Context context, String key, boolean isRemember) {
        getEditor(context).putBoolean(key, isRemember);
    }

    //To get Remember me
    public boolean getRememberMe(Context context, String key) {
        return getPreferences(context).getBoolean(key, false);
    }

    public void deletePreferences(Context context) {
        SharedPreferences preferences = getPreferences(context);
        preferences.edit().clear().apply();
    }

    public SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    public SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    /*.................................Shared Preferences - End.......................................*/
    //To Display Spinner Data
    public Spinner displaySpinnerData(Activity activity, Spinner spinner, String[] data) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(arrayAdapter);
        return spinner;
    }

    //To Display Simple List
    public ListView displaySimpleList(Activity activity, ListView listView, String[] data) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, data);
        listView.setAdapter(arrayAdapter);
        return listView;
    }

}

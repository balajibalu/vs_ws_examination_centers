package com.codetree.verification.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    Context context;
    List<?> list;
    private int currentPos = RecyclerView.NO_POSITION;


    public RecyclerViewAdapter(Context context,List<?> list) {
        this.context=context;
        this.list=list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the list item
        /*View view = LayoutInflater.from(context).inflate(R.layout.list_item,parent,false);
        return new RecyclerViewAdapter.ViewHolder(view);*/
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Binding data

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
            //initialise views by using itemView

        }
    }
}

package com.codetree.verification.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.codetree.verification.R;
import com.codetree.verification.models.localities.Masterlist;

import java.util.ArrayList;
import java.util.List;

public class LocalitiesAdapter extends ArrayAdapter {

    private Context context;
    private List<Masterlist> listOfStrings;
    private List<Masterlist> listOfStringsCopy = new ArrayList<>();


    public LocalitiesAdapter(@NonNull Context context, List<Masterlist> list) {
        super(context, R.layout.dialog_with_list_item, list);
        this.context = context;
        this.listOfStrings = list;
        this.listOfStringsCopy.addAll(listOfStrings);
    }


    static class MyViewHolder {
        TextView tv;
        public MyViewHolder(View view) {
            tv = view.findViewById(R.id.tv_list_adapetr);
        }
    }


    @Override
    public int getCount() {
        return listOfStrings.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            row = inflater.inflate(R.layout.dialog_with_list_item, parent, false);
            MyViewHolder viewHolder = new MyViewHolder(row);
            row.setTag(viewHolder);
        }
        MyViewHolder viewHolder1 = (MyViewHolder) row.getTag();
        viewHolder1.tv.setText(listOfStrings.get(position).getLOCALITYNAME());
        return row;
    }
    public void filter(String text) {
        if (text.isEmpty()) {
            listOfStrings.clear();
            listOfStrings.addAll(listOfStringsCopy);
        } else {
            ArrayList<Masterlist> result = new ArrayList<>();
            text = text.toLowerCase();
            for (Masterlist item : listOfStringsCopy) {
                if (item.getLOCALITYNAME().toLowerCase().contains(text)) {
                    result.add(item);
                }
            }
            listOfStrings.clear();
            listOfStrings.addAll(result);
        }
        notifyDataSetChanged();
    }

}

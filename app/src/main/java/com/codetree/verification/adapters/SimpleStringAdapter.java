package com.codetree.verification.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.codetree.verification.R;


import java.util.List;

public class SimpleStringAdapter extends ArrayAdapter {

    private Context context;
    private List<?> listOfStrings;

    public SimpleStringAdapter(@NonNull Context context, List<?> list) {
        super(context, R.layout.simple_list_item, list);
        this.context = context;
        this.listOfStrings = list;
    }


    static class MyViewHolder {
        TextView tv;
        public MyViewHolder(View view) {
            tv = view.findViewById(R.id.textView);
        }
    }


    @Override
    public int getCount() {
        return listOfStrings.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            row = inflater.inflate(R.layout.simple_list_item, parent, false);
            MyViewHolder viewHolder = new MyViewHolder(row);
            row.setTag(viewHolder);
        }
        MyViewHolder viewHolder1 = (MyViewHolder) row.getTag();
        return row;
    }
}

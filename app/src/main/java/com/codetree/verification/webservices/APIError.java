package com.codetree.verification.webservices;

public class APIError {
    private int statusCode;
    private String message;

    APIError() {
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }
}

package com.codetree.verification.webservices;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {
    public static APIError parseError(Response<?> response) {
        APIError error;
        Converter<ResponseBody, APIError> converter = RestAdapter.retrofit.responseBodyConverter(APIError.class, new Annotation[0]);
        try {
            assert response.errorBody() != null;
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }
        return error;
    }
}

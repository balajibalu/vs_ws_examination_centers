package com.codetree.verification.webservices;


import com.codetree.verification.models.InstituteInput;
import com.codetree.verification.models.LoginInput;
import com.codetree.verification.models.LoginResponse;
import com.codetree.verification.models.SubmitData;
import com.codetree.verification.models.SubmitResponse;
import com.codetree.verification.models.VersionModel;
import com.codetree.verification.models.institute_details.InstituteData;
import com.codetree.verification.models.localities.LocalitiesResponse;
import com.codetree.verification.models.mandals.MandalResponse;
import com.codetree.verification.models.mandals.MasterRequest;
import com.codetree.verification.models.vilages.VillagesResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiCall {

    //http://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/Getcheckversion?Version=1.0
    @GET("Getcheckversion")
    Call<VersionModel> checkversion(@Query("Version") String version);

    //http://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/GetZPLogin
    @POST("GetZPLogin")
    Call<LoginResponse> getGmLogin(@Header("Username") String Username,
                                   @Header("Password") String Password,
                                   @Header("Session_Key") String Session_Key,
                                   @Body LoginInput loginInputModel);


    //http://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/GetInstituteData
    @POST("GetInstituteData")
    Call<InstituteData> getInstituteDetails(@Header("Username") String Username,
                                            @Header("Password") String Password,
                                            @Header("Session_Key") String Session_Key,
                                            @Body InstituteInput data);

   /* //http://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/SaveCenterData
    @POST("GetInstituteData")
    Call<InstituteData> getInstituteDetails(@Body InstituteInput data);
*/

    //http://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/DMVMasterLoad
    @POST("DMVMasterLoad")
    Call<MandalResponse> getMandals(@Header("Username") String Username,
                                    @Header("Password") String Password,
                                    @Header("Session_Key") String Session_Key,
                                    @Body MasterRequest data);

    //http://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/DMVMasterLoad
    @POST("DMVMasterLoad")
    Call<VillagesResponse> getVillages(@Header("Username") String Username,
                                       @Header("Password") String Password,
                                       @Header("Session_Key") String Session_Key,
                                       @Body MasterRequest data);

    //http://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/DMVMasterLoad
    @POST("DMVMasterLoad")
    Call<LocalitiesResponse> getLocality(@Header("Username") String Username,
                                         @Header("Password") String Password,
                                         @Header("Session_Key") String Session_Key,
                                         @Body MasterRequest data);


    //http://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/SaveCenterData
    @Headers("Content-Type: application/json")
     @POST("SaveCenterData")
    Call<SubmitResponse> submit(@Header("Username") String Username,
                                @Header("Password") String Password,
                                @Header("Session_Key") String Session_Key,
                                @Body SubmitData data);


}

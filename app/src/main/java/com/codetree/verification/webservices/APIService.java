package com.codetree.verification.webservices;


public interface APIService {
    /*Dynamic Urls for Request
    @GET
    public Call<ResponseBody> profilePicture(@Url String url);*/


    /*...................Retrieve Data from Server..........................*/

    /*https://api.example.com/tasks
    @GET("tasks")
    Call<List<Task>> getTasks();*/

    /*https://api.example.com/tasks/123
    @GET("tasks/{task_id}")
    Call<Task> getTask(@Path("task_id") String taskId);*/

    /*https://api.example.com/tasks?id=123
    @GET("tasks")
    Call<Task> getTask(@Query("id") long taskId);*/

    /*http://www.appdomain.com/users?page=5&order=20
    @GET("news")
    Call<List<News>> getNews(@Query("page") int page,@Query("order") String order);*/


    /*Multiple query parameters of the same name
    https://api.example.com/tasks?id=123&id=124&id=125
    @GET("/tasks")
    List<Task> getTask(@Query("id") List<Long> taskIds);*/


    /*Retrieve Data from server using QueryMap
    @GET("/news")
    Call<List<News>> getNews( @QueryMap Map<String, String> options);*/

    /*...................Retrieve Data from Server..........................*/

    /*.........................Send Data To Server.............................*/

    /*https://api.example.com/tasks
    @POST("tasks")
    Call<Task> createTask(@Body Task task);*/

    /*https://api.example.com/tasks
    @FormUrlEncoded
    @POST("tasks")
    Call<Task> createTask(@Field("title") String title,@Field("tasks") String tasks);*/

    /*Update Data  in server by Form-Urlencoded and Field
    @FormUrlEncoded
    @PUT("user")
    Call<User> update(
            @Field("username") String username,
            @Field("name") String name,
            @Field("email") String email,
            @Field("homepage") String homepage,
            @Field("location") String location
    );*/

    /*Submit Data by using @FormUrlEncoded(),@Post(),@Field() methods
   @FormUrlEncoded
   @POST("user-login.php")
   Call<LoginModel> checkLogin(@Field("username") String uname, @Field("password") String password, @Field("device_id") String did);*/

   /* @POST("GetMPDOLogin")
   Call<LoginResponse> getGmLogin(@Body LoginInputModel loginInputModel);*/

    /*Submit Data by FOrmUrlEncoded using @FieldMap
    @FormUrlEncoded
    @PUT("user")
    Call<User> update(@FieldMap Map<String, String> fields);*/

    /*Send plain text as request body
    @POST("path")
    Call<String> getStringScalar(@Body String body);*/

    /*access Gists
    @GET("gists")
    Call<List<Gist>> getGists();*/

    /*create new Gist
    @POST("gists")
    Call<ResponseBody> createGist(@Body Gist gist);*/

    /*update existing Gist using below two methods
    @PATCH("gists/{id}")
    Call<ResponseBody> patchGist(@Path("id") String id, @Body Gist gist);

    @PUT("gists/{id}")
    Call<ResponseBody> replaceGist(@Path("id") String id, @Body Gist gist);*/

    /*delete Gist
    @DELETE("gists/{id}")
    Call<ResponseBody> deleteGist(@Path("id") String id);*/
    /*.........................Send Data To Server.............................*/

    /*.......................................Headers.........................*/

    /*Static Request Headers
    @Headers({
        "Accept: application/vnd.yourapi.v1.full+json",
        "User-Agent: Your-App-Name"
    })
    @GET("/tasks/{task_id}")
    Call<Task> getTask(@Path("task_id") long taskId);*/

    /*Dynamic Request Headers
    @GET("/tasks")
    Call<List<Task>> getTasks(@Header("Content-Range") String contentRange);*/

    /*Request Headers using @HeaderMap
    @GET("/tasks")
    Call<List<Task>> getTasks(@HeaderMap Map<String, String> headers);*/
    /*.......................................Headers.........................*/

}
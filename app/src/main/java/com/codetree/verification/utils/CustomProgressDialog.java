package com.codetree.verification.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.codetree.verification.R;


public class CustomProgressDialog extends AlertDialog {

    Context context;

    public CustomProgressDialog(Context context) {
        this(context, R.style.ProgressDialogTheme);
        this.context = context;
    }


    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_view);
        setCanceledOnTouchOutside(false);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_view);
        if(progressBar!=null){
            progressBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_IN);
        }

    }
}

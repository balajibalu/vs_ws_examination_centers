package com.codetree.verification.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

public class PrefUtils {

    private static final PrefUtils prefUtils = new PrefUtils();
    private static SharedPreferences myPrefs = null;

    public static PrefUtils getInstance(Context context) {
        if (null == myPrefs)
            myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefUtils;
    }

    public boolean hasKey(String key) {
        return myPrefs.contains(key);
    }

    // Shared Preferences Functions
    public void saveString(String key, String value) {
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(String key) {
        return myPrefs.getString(key, null);
    }

    public void saveInt(String key, int value) {
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getInt(String key) {
        return myPrefs.getInt(key, 0);
    }

    public void saveBool(String key, boolean value) {
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBool(String key) {
        return myPrefs.getBoolean(key, false);
    }

    public void saveFloat(String key, float value) {
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public float getFloat(String key) {
        return myPrefs.getFloat(key, 0);
    }

    public void saveLong(String key, long value) {
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public long getLong(String key) {
        return myPrefs.getLong(key, 0);
    }

    public void saveSet(String key, Set<String> value) {
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.putStringSet(key, value);
        editor.apply();
    }

    public Set<String> getSet(String key) {
        return myPrefs.getStringSet(key, null);
    }

    public void clear() {
        myPrefs.edit().clear().apply();
    }
}

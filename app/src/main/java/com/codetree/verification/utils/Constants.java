package com.codetree.verification.utils;

import android.net.Uri;

import static android.content.Context.MODE_PRIVATE;

public class Constants {
    //to identify sending data between activities
    public static final String EXTRA_DATA="extra data";
    public static final String EXTRA_NAME="name";
    public static final String SHARED_PREFERENCE_FILE_NAME="preferences";
    public static int SHARED_PREFERENCE_FILE_MODE=MODE_PRIVATE;
    public static final String ONE_TIME_LOGIN="one time login";

    //Constants for Restful web services
    public static final String BASE_URL="https://gramavolunteer2.ap.gov.in/GVInterviewApp/API/GSCenters/";
    public static final boolean hadPhotoStore = true;
    public static final boolean showPhotoStore = false;
    private static Uri imageUri;





    public static final String LOGIN_TOKEN = "login_code";
    public static final String DISTRICT_NAME = "districtName";
    public static final String MANDAL_NAME = "mandalName";
    public static final String DEPARTMENT = "department";
    public static final String DESIGNATION = "designation";
    public static final String NAME = "name";
    public static final String MOBILE_NUM = "mobile";
    public static final String DISTRICT_CODE = "districtCode";
    public static final String MANDAL_CODE = "mandalCode";
    public static final String RURAL_URBAN = "r_or_u";
    public static final String REMEMBER = "remember";
    public static final String Username = "gramavolunteer";
    public static final String Password = "gv@789";
    public static final String Session_Key = "969A687C0F273H0756A89347529CF5D377000995FC1B9GG97CD0TYU07A215ISGV";



}

package com.codetree.verification.utils;

import android.app.Activity;
import android.content.Context;
import android.view.WindowManager;

public class SPSProgressDialog {
    private static CustomProgressDialog mProgressDialog;

    public static void showProgressDialog(Activity context) {
        dismissProgressDialog();
        mProgressDialog = new CustomProgressDialog(context);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setCancelable(true);
        mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mProgressDialog.show();
    }
    public static void showProgressDialog(Context context) {
        dismissProgressDialog();
        mProgressDialog = new CustomProgressDialog(context);
        mProgressDialog.setCancelable(true);
        mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mProgressDialog.show();
    }

    public static void dismissProgressDialog() {
        try {
            if (null != mProgressDialog) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

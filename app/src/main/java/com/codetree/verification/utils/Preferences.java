package com.codetree.verification.utils;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.codetree.verification.base.BaseApp;


public class Preferences {
    private static Preferences pref;
    private SharedPreferences prefObj;
    private SharedPreferences prefObjforCount;

    private Editor prefsEditor;

    private Preferences(SharedPreferences prefObj) {
        //default constructor needed
        this.prefObj = prefObj;
        this.prefObjforCount = prefObj;
        prefsEditor = prefObj.edit();

    }

    public synchronized static Preferences getIns() {
        if (pref == null) {
            pref = new Preferences(PreferenceManager.getDefaultSharedPreferences(BaseApp.get()));
        }
        return pref;
    }

    public String getLoginToken() {
        return prefObj.getString(Constants.LOGIN_TOKEN, "");
    }

    public void setLoginToken(String token) {
        prefsEditor.putString(Constants.LOGIN_TOKEN, token).commit();
    }

    public String getDistrictName() {
        return prefObj.getString(Constants.DISTRICT_NAME, "");
    }

    public void setDistrictName(String districtName) {
        prefsEditor.putString(Constants.DISTRICT_NAME, districtName).commit();
    }

    public String getMandaltName() {
        return prefObj.getString(Constants.MANDAL_NAME, "");
    }

    public void setMandalName(String mandalName) {
        prefsEditor.putString(Constants.MANDAL_NAME, mandalName).commit();
    }

    public String getDepartment() {
        return prefObj.getString(Constants.DEPARTMENT, "");
    }

    public void setDepartment(String department) {
        prefsEditor.putString(Constants.DEPARTMENT, department).commit();
    }

    public String getDesignation() {
        return prefObj.getString(Constants.DESIGNATION, "");
    }

    public void setDesignation(String designation) {
        prefsEditor.putString(Constants.DESIGNATION, designation).commit();
    }

    public String getName() {
        return prefObj.getString(Constants.NAME, "");
    }

    public void setName(String name) {
        prefsEditor.putString(Constants.NAME, name).commit();
    }

    public String getMobileNo() {
        return prefObj.getString(Constants.MOBILE_NUM, "");
    }

    public void setMobileNo(String mobileNo) {
        prefsEditor.putString(Constants.MOBILE_NUM, mobileNo).commit();
    }

    public String getDistrictCode() {
        return prefObj.getString(Constants.DISTRICT_CODE, "");
    }

    public void setDistrictCode(String districtCode) {
        prefsEditor.putString(Constants.DISTRICT_CODE, districtCode).commit();
    }

    public String getMandalCode() {
        return prefObj.getString(Constants.MANDAL_CODE, "");
    }

    public void setMandalCode(String districtCode) {
        prefsEditor.putString(Constants.MANDAL_CODE, districtCode).commit();
    }

    public String getRorU() {
        return prefObj.getString(Constants.RURAL_URBAN, "");
    }

    public void setRoU(String roU) {
        prefsEditor.putString(Constants.RURAL_URBAN, roU).commit();
    }


    public boolean getRemeberMe() {
        return prefObj.getBoolean(Constants.REMEMBER, false);
    }

    public void setRememberMe(boolean rememberMe) {
        prefsEditor.putBoolean(Constants.REMEMBER, rememberMe).commit();
    }


    public void clear() {
        prefObj.edit().clear().commit();
        prefObjforCount.edit().clear().commit();
    }
}
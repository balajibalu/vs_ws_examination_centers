package com.codetree.verification.utils;

import android.graphics.Bitmap;
import android.net.Uri;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 20-06-2018.
 */

public class RequestCameraObj {
    private Bitmap bitmap;
    @SerializedName("Base64")
    private String basephoto;
    private Uri imageUri;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getBasephoto() {
        return basephoto;
    }

    public void setBasephoto(String basephoto) {
        this.basephoto = basephoto;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }
}

package com.codetree.verification.utils;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;


public class ImageUtils {

    public static void setBase64(ImageView imageView, String base64){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imageView.setImageBitmap(decodedByte);
    }

    public static Bitmap scaleBitmap(Bitmap bm) {
        int maxWidth = 512, maxHeight = 512, maxHeightPotrait = 540;
        int width = bm.getWidth();
        int height = bm.getHeight();
        Log.v("Pictures", "Width and height are: " + width + " : " + height);
        if (width > height) {
            // landscape
            int ratio = width / maxWidth;
            if (ratio <= 0) {
                return bm;
            } else {
                height = height / ratio;
            }
            if (width > maxWidth) {
                width = maxWidth;
            }
        } else if (height > width) {
            // portrait
            int ratio = height / maxHeightPotrait;
            if (ratio <= 0) {
                return bm;
            } else {
                width = width / ratio;
            }
            if (height > maxHeightPotrait) {
                height = maxHeightPotrait;
            }
        } else {
            // square
            if (height > maxHeight) {
                height = maxHeight;
                width = maxWidth;
            }
        }
        Log.v("Pictures", "after scaling Width and height are: " + width + " : " + height);
        bm = Bitmap.createScaledBitmap(bm, width, height, true);
        return bm;
    }

    public static Bitmap processingBitmap(Bitmap bm1, String caption, String captionString) {
        Bitmap newBitmap = null;
        Bitmap.Config config = bm1.getConfig();
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        Log.d("BitMap:", bm1.getHeight() + "* " + bm1.getWidth());
        newBitmap = Bitmap.createBitmap(bm1.getWidth(), bm1.getHeight(), config);
        Canvas newCanvas = new Canvas(newBitmap);
        newCanvas.drawBitmap(bm1, 0, 0, null);

        if (captionString != null) {
            Rect rectText = new Rect();
            Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintText.setColor(Color.WHITE);
            paintText.setTextSize(15);
            paintText.setStyle(Paint.Style.FILL);
            paintText.setShadowLayer(10f, 10f, 10f, Color.BLACK);
            paintText.getTextBounds(captionString, 0, captionString.length(), rectText);
            newCanvas.drawText(caption, 5f, bm1.getHeight() - 70f, paintText);
            newCanvas.drawText(captionString, 5f, bm1.getHeight() - 40f, paintText);
            newCanvas.drawText("Time Stamp: " + getTimeStamp(), 5f, bm1.getHeight() - 15f, paintText);
        }
        return newBitmap;
    }

    public static Bitmap getBitmap(String selectedImagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = false;

        Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(selectedImagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        int angle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            angle = 90;
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            angle = 180;
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            angle = 270;
        }
        Matrix mat = new Matrix();
        mat.postRotate(angle);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mat, true);
        return bitmap;
    }

    public static String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public static Bitmap getCircularBitmapImage(Activity a, Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());
        System.out.println("Image Width/Height Size: " + size);
        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;
        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);

        Bitmap bitmap = Bitmap
                .createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);
        float radius = size / 2f;
        canvas.drawCircle(radius, radius, radius, paint);
        return bitmap;
    }
    public static String getTimeStamp() {
        String timeStamp = "";
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        String year = "" + cal.get(Calendar.YEAR);
        String month = "" + (cal.get(Calendar.MONTH) + 1);
        if (month.length() == 1) {
            month = "0" + month;
        }
        String day = "" + cal.get(Calendar.DAY_OF_MONTH);
        if (day.length() == 1) {
            day = "0" + day;
        }
        String hour = "" + cal.get(Calendar.HOUR_OF_DAY);
        if (hour.length() == 1) {
            hour = "0" + hour;
        }
        String min = "" + cal.get(Calendar.MINUTE);
        if (min.length() == 1) {
            min = "0" + min;
        }
        String sec = "" + cal.get(Calendar.SECOND);
        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        timeStamp = day + "-" + month + "-" + year + ", " + hour + ":" + min + ":" + sec;
        return timeStamp;
    }

//    public void onPictureTaken(byte[] data) {
//        // Find out if the picture needs rotating by looking at its Exif data
//        ExifInterface exifInterface = new ExifInterface(new ByteArrayInputStream(data));
//        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//        int rotationDegrees = 0;
//        switch (orientation) {
//            case ExifInterface.ORIENTATION_ROTATE_90:
//                rotationDegrees = 90;
//                break;
//            case ExifInterface.ORIENTATION_ROTATE_180:
//                rotationDegrees = 180;
//                break;
//            case ExifInterface.ORIENTATION_ROTATE_270:
//                rotationDegrees = 270;
//                break;
//        }
//        // Create and rotate the bitmap by rotationDegrees
//    }
}
